﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class UserStaff:BaseEntity
    {
        public string FullName { get; set; }
        public DateTime Dateofbirth { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public virtual User1 User1 { get; set; }

    }
}
