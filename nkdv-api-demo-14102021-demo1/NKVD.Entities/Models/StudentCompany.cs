﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class StudentCompany : BaseEntity
    {
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public int CompanyId { get; set; }
        public Company Company { get; set; }
        
    }
}
