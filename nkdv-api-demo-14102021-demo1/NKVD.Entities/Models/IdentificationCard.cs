﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class IdentificationCard : BaseEntity
    {
        public string IdIdentificationCard { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string IssuedBy { get; set; }
        // 0: Female; 1: Male
        public byte Sex { get; set; }
        public string PermanentAddress { get; set; }
        public string CurrentAddress { get; set; }
        public string HometownAddress { get; set; }
        public DateTime Birthday { get; set; }
        public string BirthPlace { get; set; }
        
        public virtual Student Student { get; set; }
        public virtual Guardian Guardian { get; set; }

    }
}
