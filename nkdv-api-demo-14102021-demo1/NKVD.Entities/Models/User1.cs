﻿
using System.Collections.Generic;

using System.Text.Json.Serialization;


namespace NKVD.Entities.Models
{
    public class User1 : BaseEntity
    {
        public int? StudentID { get; set; }
        public int? UserAdminID { get; set; }
        public int? TeacherID { get; set; }
        public int? UserStaffID { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public Role Role { get; set; }
        public string? Token { get; set; }
        public virtual UserStaff? UserStaff { get; set; }
        public virtual UserAdmin? UserAdmin { get; set; }
        public virtual Teacher? Teacher { get; set; }
        [JsonIgnore]
        public string PasswordHash { get; set; }

        [JsonIgnore]
        public List<RefreshToken1> RefreshTokens1 { get; set; }
        public virtual Student Student { get; set; }
    //   public virtual Teacher Teacher { get; set; }
    }
}
