﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class DetailedCost : BaseEntity
    {
        public string Submitter { get; set; }
        public string Collector { get; set; }
        public DateTime DateOfLing { get; set; }
        public string Type { get; set; }
        public double Piece { get; set; }
        public string Note { get; set; }
        //foreign key
        public virtual int CostOfStudentId { get; set; }
        public virtual Student Student { get; set; }
        public IList<TypeCost> TypeCosts { get; set; }
    }
    
}
