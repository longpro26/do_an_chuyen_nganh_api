﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class TeacherGrade
    {
        public int TeacherId { get; set; }
        public Teacher Teacher { get; set; }
        public int GradeId { get; set; }
        public Grade Grade { get; set; }
    }
}
