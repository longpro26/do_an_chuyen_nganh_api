﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class Teacher : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
      //  public int UserId { get; set; }
        public IList<TeacherGrade> TeacherGrades { get; set; }
        public IList<Review> Reviews { get; set; }
        //     public virtual User1? User { get; set; }
        public virtual User1 User1 { get; set; }


    }
}
