﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class Student : BaseEntity
    {
        public string PhoneNumber { get; set; }
        public string FacebookUrl { get; set; }
        public string Gmail { get; set; }
        public int IdentificationCardOfStudentId { get; set; }
       // public int UserId { get; set; }
        public virtual IdentificationCard? IdentificationCard { get; set; }
      //  public virtual User1? User { get; set; }
        public virtual Guardian Guardian { get; set; }
        public virtual ICollection<FamilyMember> FamilyMembers { get; set; }
        public virtual InfoInJapan InfoInJapan { get; set; }
        public virtual ICollection<Document> Documents { get; set; }
        public virtual Visa Visa { get; set; }
        public IList<DetailedCost>  DetailedCosts { get; set; }
        public IList<StudentCompany> StudentCompanys { get; set; }
        public virtual ICollection<PassEntryHistory> PassEntryHistorys { get; set; }
        public virtual ICollection<WorkExperience> WorkExperiences { get; set; }
        public virtual ICollection<EduBackground> EduBackgrounds { get; set; }
        public IList<StudentGrade> StudentGrades { get; set; }
        public IList<Review> Reviews { get; set; }
        public virtual Cost Cost { get; set; }
    }
}
