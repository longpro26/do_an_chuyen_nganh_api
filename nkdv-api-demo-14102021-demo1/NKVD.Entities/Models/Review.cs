﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class Review : BaseEntity
    {
        public int TeacherId { get; set; }
        public Teacher Teacher { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public float Score { get; set; }
        public string LearningAttitude { get; set; }
        public string HomeworkRate { get; set; }
        public string AttendanceRate { get; set; }
    }
}
