﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class Document : BaseEntity
    {
        public string Name { get; set; }
        public string Link { get; set; }
        public string Submitter { get; set; }
        public string Receiver { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string Note { get; set; }
        public string Type { get; set; }

        //foreign key
        public Student Student { get; set; }
        public int? StudentId { get; set; }
    }
}
