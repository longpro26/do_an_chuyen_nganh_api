﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class WorkExperience : BaseEntity
    {
        public string WorkName { get; set; }
        public string Address { get; set; }
        public string Position { get; set; }
        public DateTime Form { get; set; }
        public DateTime To { get; set; }
        //foreign key
        public Student Student { get; set; }
        public int? StudentId { get; set; }
    }
}
