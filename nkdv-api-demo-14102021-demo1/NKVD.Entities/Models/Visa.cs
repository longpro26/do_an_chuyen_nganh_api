﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class Visa
    {
        [Key]
        public int Id { get; set; }
        public DateTime DateRange { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string IssuedByVisa { get; set; }
        public string Type { get; set; }
        public virtual int VisaOfStudentId { get; set; }
        public virtual Student Student { get; set; }

    }
}
