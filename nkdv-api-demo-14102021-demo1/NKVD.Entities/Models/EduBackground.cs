﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class EduBackground : BaseEntity
    {
        public string SchoolName { get; set; }
        public DateTime Form { get; set; }
        public DateTime To { get; set; }
        public string Address { get; set; }
        //foreign key
        public Student Student { get; set; }
        public int? StudentId { get; set; }
    }
}
