﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class FamilyMember : BaseEntity
    {
        public string Relationship { get; set; }
        public DateTime BirthDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Job { get; set; }
        public string Address { get; set; }
        public Student Student { get; set; }
        public int? StudentId { get; set; }
        public string Name { get; set; }

    }
}
