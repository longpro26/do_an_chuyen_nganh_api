﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class Cost : BaseEntity
    {   
        public double TotalPrice { get; set; }
        public DateTime ContractSigningDate { get; set; }
        public string Note { get; set; }
        public virtual int TotalCostOfStudentId { get; set; }
        public virtual Student Student { get; set; }
    }
}
