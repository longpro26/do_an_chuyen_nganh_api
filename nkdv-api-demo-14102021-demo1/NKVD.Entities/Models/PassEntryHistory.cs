﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class PassEntryHistory
    {
        public int Id { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime OutputDate { get; set; }
        public string PassportValidity { get; set; }
        public string Purpose { get; set; }

        //foreign key
        public Student Student { get; set; }
        public int? StudentId { get; set; }
    }
}
