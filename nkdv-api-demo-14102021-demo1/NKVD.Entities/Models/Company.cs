﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class Company : BaseEntity
    {
        public string Name { get; set; }
        public string BusinessProfession { get; set; }
        public string TaxCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string Representative { get; set; }
        public virtual Guardian Guardian { get; set; }
        public IList<StudentCompany> StudentCompanys { get; set; }

    }
}
