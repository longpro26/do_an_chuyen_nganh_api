﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class InfoInJapan : BaseEntity
    {
        public string Address { get; set; }
        public string AreaCode { get; set; }
        public string AddressType { get; set; }
        public string Majors { get; set; }
        public string Specialized { get; set; }
        public string SchoolName { get; set; }
        public int InfoInJapanOfStudentId { get; set; }
        public virtual Student Student { get; set; }
    }
}
