﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class Guardian : BaseEntity
    {
        public string WorkPlace { get; set; }
        public string PhoneNumber { get; set; }
        public string RepresentativePosition { get; set; }
        public DateTime WorkingTime { get; set; }
        public string GuardianPosition { get; set; }
        public string TaxCode { get; set; }

        //foreign key
        // studenid
        public int? GuardianOfStudentId { get; set; }

        public virtual Student Student { get; set; }

        // comany id
        public int? CompanyOfGuardianId { get; set; }
        public virtual Company Company { get; set; }
        // id code
        public int? IdentificationCardOfGuardianId { get; set; }
        public virtual IdentificationCard IdentificationCard { get; set; }


    }
}
