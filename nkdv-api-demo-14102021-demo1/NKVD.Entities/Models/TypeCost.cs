﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class TypeCost : BaseEntity
    {
        public int TypeId { get; set; }
        public Type Types { get; set; }
        public int DetailCostId { get; set; }
        public DetailedCost DetailedCosts { get; set; }
    }
}
