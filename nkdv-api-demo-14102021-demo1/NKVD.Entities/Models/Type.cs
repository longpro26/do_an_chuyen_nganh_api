﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class Type :BaseEntity
    {
        public string Name { get; set; }
        public IList<TypeCost> TypeCosts { get; set; }
    }
}
