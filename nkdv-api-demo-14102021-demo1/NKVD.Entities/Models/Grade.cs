﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Models
{
    public class Grade : BaseEntity
    {
        public string Name { get; set; }
        public DateTime OpenDate { get; set; }
        public IList<StudentGrade> StudentGrades { get; set; }
        public IList<TeacherGrade> TeacherGrades { get; set; }
    }
}
