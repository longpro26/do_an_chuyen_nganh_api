﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NKVD.Entities.Context
{
    public class NKDVContext : DbContext
    {
        public NKDVContext(DbContextOptions<NKDVContext> ops) : base(ops)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer(Configuration.GetConnectionString("NKDVConnection"));
                optionsBuilder.UseSqlServer("Server=.;Database=NKDV;Trusted_Connection=True;");

            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Configure Student & StudentAddress entity// Configure Student & StudentAddress entity
            //one to one
            modelBuilder.Entity<IdentificationCard>()
                .HasOne<Student>(i => i.Student)
                .WithOne(s => s.IdentificationCard)
                .HasForeignKey<Student>(s => s.IdentificationCardOfStudentId);
            modelBuilder.Entity<IdentificationCard>()
                .HasOne<Guardian>(i => i.Guardian)
                .WithOne(g => g.IdentificationCard)
                .HasForeignKey<Guardian>(g => g.IdentificationCardOfGuardianId);
            modelBuilder.Entity<Student>()
                .HasOne<Guardian>(s => s.Guardian)
                .WithOne(g => g.Student)
                .HasForeignKey<Guardian>(g => g.GuardianOfStudentId);
            modelBuilder.Entity<Company>()
                .HasOne<Guardian>(c => c.Guardian)
                .WithOne(g => g.Company)
                .HasForeignKey<Guardian>(g => g.CompanyOfGuardianId);
            modelBuilder.Entity<Student>()
                .HasOne<InfoInJapan>(s => s.InfoInJapan)
                .WithOne(i => i.Student)
                .HasForeignKey<InfoInJapan>(s => s.InfoInJapanOfStudentId);
            modelBuilder.Entity<Student>()
                .HasOne<Visa>(s => s.Visa)
                .WithOne(v => v.Student)
                .HasForeignKey<Visa>(v => v.VisaOfStudentId);
            modelBuilder.Entity<Student>()
                .HasOne<Cost>(s => s.Cost)
                .WithOne(v => v.Student)
                .HasForeignKey<Cost>(v => v.TotalCostOfStudentId);

            //one to many
            modelBuilder.Entity<Student>()
                .HasMany<FamilyMember>(objStudent => objStudent.FamilyMembers)
                .WithOne(objFamilyMenber => objFamilyMenber.Student)
                .HasForeignKey(objFamilyMenber => objFamilyMenber.StudentId);
            modelBuilder.Entity<Student>()
                .HasMany<Document>(objStudent => objStudent.Documents)
                .WithOne(objDocument => objDocument.Student)
                .HasForeignKey(objFamilyMenber => objFamilyMenber.StudentId);
            modelBuilder.Entity<Student>()
                .HasMany<PassEntryHistory>(objStudent => objStudent.PassEntryHistorys)
                .WithOne(p => p.Student)
                .HasForeignKey(p => p.StudentId);
            modelBuilder.Entity<Student>()
                .HasMany<WorkExperience>(objStudent => objStudent.WorkExperiences)
                .WithOne(w => w.Student)
                .HasForeignKey(w => w.StudentId);
            modelBuilder.Entity<Student>()
               .HasMany<DetailedCost>(objStudent => objStudent.DetailedCosts)
               .WithOne(w => w.Student)
               .HasForeignKey(w => w.CostOfStudentId);
            modelBuilder.Entity<Student>()
               .HasMany<EduBackground>(objStudent => objStudent.EduBackgrounds)
               .WithOne(w => w.Student)
               .HasForeignKey(w => w.StudentId);

            //many to many
            modelBuilder.Entity<StudentCompany>().HasKey(sc => new { sc.StudentId, sc.CompanyId });
            modelBuilder.Entity<StudentGrade>().HasKey(sc => new { sc.StudentId, sc.GradeId });
            modelBuilder.Entity<TeacherGrade>().HasKey(sc => new { sc.TeacherId, sc.GradeId });
            modelBuilder.Entity<Review>().HasKey(sc => new { sc.TeacherId, sc.StudentId });
        }
        public DbSet<Student> Students { get; set; }
        public DbSet<IdentificationCard> IdentificationCards { get; set; }
        public DbSet<Guardian> Guardians { get; set; }
        public DbSet<EduBackground> EduBackgrounds { get; set; }
        public DbSet<FamilyMember> FamilyMembers { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<InfoInJapan> InfoInJapans { get; set; }
        public DbSet<Visa> Visas { get; set; }
        public DbSet<Company> Companys { get; set; }
        public DbSet<Cost> Costs { get; set; }
        public DbSet<DetailedCost> DetailedCosts { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<PassEntryHistory> PassEntryHistorys { get; set; }
        public DbSet<StudentCompany> StudentCompanys { get; set; }
        public DbSet<StudentGrade> StudentGrades { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<TeacherGrade> TeacherGrades { get; set; }
        public DbSet<WorkExperience> WorkExperiences { get; set; }

    }
}
