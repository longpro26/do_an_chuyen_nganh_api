﻿using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.API.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Authorization;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class DocumentController : ControllerBase
    {
        private IFirebaseStorageService _firebaseStorageService;
        private IDocumentService _documentService;

        public DocumentController(IFirebaseStorageService firebaseStorageService, IDocumentService documentService)
        {
            _firebaseStorageService = firebaseStorageService;
            _documentService = documentService;
        }
        [HttpGet("getall")]
        public IActionResult GetAll()
        {
            var documents = _documentService.GetAll();
            return Ok(documents);
        }

        [HttpPost("Search")]
        public IActionResult GetDocumentsByStudentName(StudentNameSearchRequest name)
        {
            if (name == null) return Ok(); 
            var documents = _documentService.GetDocumentsByStudentName(name);
            return Ok(documents);
        }

        [HttpPost("Delete")]
        public IActionResult DeleteDocument(DocumentRequest obj)
        {
            var documents = _documentService.DeleteDocument(obj);
            return Ok(documents);
        }
        [HttpPost("Filter")]
        public IActionResult FilterDocument(FilterDocumentRequest obj)
        {
            var documents = _documentService.FilterDocument(obj);
            return Ok(documents);
        }
        [HttpPost("Edit")]
        public async Task<IActionResult> UpdateDocument([FromForm] UploadFileRequest request)
        {
            string fileName = null;
            if (request.FileProfile != null)
            {
                var fileContent = request.FileProfile.OpenReadStream();
                var extensionFile = Path.GetExtension(request.FileProfile.FileName);
                fileName = await _firebaseStorageService.PutFileToFirebaseAsync(fileContent, extensionFile);
            }
            DateTime fromDate = Convert.ToDateTime(request.ReceivedDate).Date;
            var obj = new DataDocumentRequest()
            {
                Id = request.Id,
                Name = request.Name,
                Receiver = request.Receiver,
                ReceivedDate = fromDate,
                Submitter = request.Submitter,
                Note = request.Note,
                StudentId = request.StudentId,
                Type = request.Type,
                FileProfile = fileName
            };
            var flgUpdate = _documentService.UpdateDocument(obj);
            if (flgUpdate)
            {
                var documents = _documentService.GetAll();
                return Ok(documents);
            }
            return Ok(flgUpdate);
        }
        //[AllowAnonymous]
        //[HttpPost("AddDocument")]
        //public IActionResult CreateDocument(DataDocumentRequest obj)
        //{
        //    var documents = _documentService.CreateDocument(obj);
        //    return Ok(documents);
        //}
        [HttpPost("AddDocument")]
        public async Task<IActionResult> CreateDocument([FromForm] UploadFileRequest request)
                {
            var fileContent = request.FileProfile.OpenReadStream();
            var extensionFile = Path.GetExtension(request.FileProfile.FileName);
            var fileName = await _firebaseStorageService.PutFileToFirebaseAsync(fileContent, extensionFile);
            DateTime fromDate = Convert.ToDateTime(request.ReceivedDate).Date;
            var obj = new DataDocumentRequest()
            {
                Id = request.Id,
                Name = request.Name,
                Receiver = request.Receiver,
                ReceivedDate = fromDate,
                Submitter = request.Submitter,
                Note = request.Note,
                StudentId = request.StudentId,
                Type = request.Type,
                FileProfile = fileName
            };
            var documents = _documentService.CreateDocument(obj);

            return Ok();
            // Store fileName to database
        }

        [HttpGet("Receiver")]
        public IActionResult GetReceiver()
        {
            var documents = _documentService.GetReceiver();
            return Ok(documents);
        }
        [HttpGet("StudentName")]
        public IActionResult GetStudentName()
        {
            var documents = _documentService.GetStudentName();
            return Ok(documents);
        }
        [HttpGet("Type")]
        public IActionResult GetType()
        {
            var documents = _documentService.GetType();
            return Ok(documents);
        }
        [HttpPost("download-file/{id}")]
        public IActionResult Download(int Id, TypeExportRequets typeExport)
        {
            var fileExport = _documentService.GetWordDocumentFromTemplateWithTempFile(Id, typeExport);

            return File(fileExport.MemoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", fileExport.FileName);
        }
    }
}
