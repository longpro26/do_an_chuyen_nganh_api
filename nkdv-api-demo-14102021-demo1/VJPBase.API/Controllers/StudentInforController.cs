﻿using Microsoft.AspNetCore.Mvc;
using NKVD.API.Services;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Authorization;
using VJPBase.API.Models.Requests;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class StudentInforController : Controller
    {
        private IStudentService _studentService;

        public StudentInforController (IStudentService studentService)
        {
            _studentService = studentService;
        }

        [Authorize(Role.Admin)]
        [HttpGet("get-student/{id}")]
        public IActionResult GetStudentById(int id)
        {
            var students = _studentService.GetbyID(id);
            return Ok(students);
        }

        [HttpGet("info-student/{id}")]
        public IActionResult GetInfoById(int id)
        {
            var students = _studentService.GetbyID(id);
            return Ok(students);
        }

        [HttpGet("get-studentinfo/{id}")]
        public IActionResult GetStudentByIdInfo(int id)
        {
            var students = _studentService.GetbyID(id);
            return Ok(students);
        }
     
        [Authorize(Role.Admin)]
        [HttpPost("create-student")]
        public IActionResult CreatStudent(StudentRequest model)
        {
            var result = _studentService.CreateStudent(model);
            return Ok(result);
        }
        [Authorize(Role.Admin)]
        [HttpPut("update-student/{stuid}")]
        public IActionResult CreatStudent(int stuid, StudentRequest model)
        {
            var result = _studentService.UpdateStudent(stuid,model);
            return Ok(result);
        }
    }
}
