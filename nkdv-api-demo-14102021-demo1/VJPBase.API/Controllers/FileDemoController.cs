﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Services;
using System;
using System.IO;
using System.Threading.Tasks;
using VJPBase.API.Authorization;
using VJPBase.API.Entities;
using VJPBase.API.Models.Requests;
using VJPBase.API.Services;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class FileDemoController: ControllerBase
    {
        private IFirebaseStorageService _firebaseStorageService;
        private IDocumentService _documentService;

        public FileDemoController(IFirebaseStorageService firebaseStorageService, IDocumentService documentService)
        {
            _firebaseStorageService = firebaseStorageService;
            _documentService = documentService;
        }

        [HttpPost("upload-file")]
        public async Task<IActionResult> UploadFileAsync([FromForm] UploadFileRequest request)
        {
            var fileContent = request.FileProfile.OpenReadStream();
            var extensionFile = Path.GetExtension(request.FileProfile.FileName);
            var fileName = await _firebaseStorageService.PutFileToFirebaseAsync(fileContent, extensionFile);

            //request.Document.Link = fileName;

            return Ok();
            // Store fileName to database
        }


        [HttpPost("download-file")]
        public async Task<IActionResult> DownloadFileAsync(DownloadFileRequest request)
        {
            var fileDto = await _firebaseStorageService.DownloadFileFromFireBaseAsync(request.Url);
            return File(fileDto.Content.ToArray(), fileDto.ContentType);
        }
    }
}
