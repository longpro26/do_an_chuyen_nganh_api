﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Services;
using System;
using VJPBase.API.Authorization;
using VJPBase.API.Entities;
using VJPBase.API.Models.Requests;
using VJPBase.API.Services;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class FamilyInformationController : ControllerBase
    {
        private IFamilyInformation _familyInformation;

        public FamilyInformationController(IFamilyInformation FamilyInformation)
        {
            _familyInformation = FamilyInformation;
        }

        [HttpGet("family_information/{StudentId}")]
        public IActionResult Familyinformation(int StudentId)
        {
            var response = _familyInformation.Familyinformationn(StudentId);
            return Ok(response);
        }

        [HttpDelete("delete_detailedcost/{Id}")]
        public IActionResult DeleteFamilyinformation(int Id)
        {
            var result = _familyInformation.DeleteFamilyinformationn(Id);
            return Ok(result);
        }

        [HttpPost("update_family_information")]
        public IActionResult UpdateFamilyinformation(UpdateFamilyInformationRequest mode)
        {
            var respone = _familyInformation.UpdateFamilyinformationn(mode);
            return Ok(respone);
        }

        [HttpPost("create_family_information/{StudentId}")]
        public IActionResult CreateFamilyinformation(int StudentId, AddFamilyInformationRequest mode)
        {
            var respone = _familyInformation.CreateFamilyinformationn(StudentId,mode);
            return Ok(respone);
        }

        [HttpPost("create_family_information")]
        public IActionResult CreateFamilyinformation(AddFamilyInformationRequest mode)
        {
            var respone = _familyInformation.CreateFamilyinformationn(mode.StudentId, mode);
            return Ok(respone);
        }
    }
}
