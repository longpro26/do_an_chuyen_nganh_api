﻿using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Authorization;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class EduBackgroundController : Controller
    {
        private IEduBackgroundService _eduService;
        public EduBackgroundController(IEduBackgroundService entryService)
        {
            _eduService = entryService;
        }

        [HttpGet("get-list-edubackground/{idst}")]
        public IActionResult GetListEduByIdStudent(int idst)
        {
            var listEdu = _eduService.GetListEduBackgroud(idst);
            return Ok(listEdu);
        }

        [HttpGet("get-edubackground/{eduId}")]
        public IActionResult GetEdu(int eduId)
        {
            var result = _eduService.GetBackgroundbyId(eduId);
            return Ok(result);
        }

        [HttpPost("create-edubackground/{idstu}")]
        public IActionResult CreateEdu(int idstu,EduBackgroundRequest model)
        {
            var result = _eduService.CreateBackground(idstu,model);
            return Ok(result);
        }

        [HttpPut("update-edubackground/{eduId}")]
        public IActionResult UpdateEdu(int eduId,EduBackgroundRequest model)
        {
            var result = _eduService.UpdateBackgroundbyId(eduId, model);
            return Ok(result);
        }

        [HttpDelete("delete-edubackground/{eduId}")]
        public IActionResult DeleteEntry(int eduId)
        {
            var result = _eduService.DeleteBackgroundbyId(eduId);
            return Ok(result);
        }
    }
}
