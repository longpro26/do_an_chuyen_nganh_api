﻿using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Models.Response.Dto;
using NKVD.API.Services;
using System.Threading.Tasks;
using VJPBase.API.Authorization;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class SendMailController : Controller
    {
        private ISendMailService _sendmail;
        private IForgotPassword _forgotpassword;
        public SendMailController( ISendMailService sendmail, IForgotPassword forgotpassword)
        {
            _sendmail = sendmail;
            _forgotpassword = forgotpassword;
        }
        [AllowAnonymous]
        [HttpPost("send-mail")]
        public async Task<IActionResult> SendMailResetPass(ForgotPasswordEmailRequest request)
        {
            bool check = false;
            string code = _forgotpassword.SendCode(request);
            if (code!="error")
            {
                MailContent content = new MailContent
                {
                    To = request.email,
                    Subject = "Đặt lại mật khẩu của bạn",
                    Body = _sendmail.createEmailBody(request.email,code)
                };
                var kq = await _sendmail.SendMail(content);
                check = true;
            } 
            return Ok(check);
        }

        [AllowAnonymous]
        [HttpGet("confirm")]
        public IActionResult XacnhanUser(string email,string token)
        {
            var response = _forgotpassword.XacnhanUser(email,token);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost("resetpass")]
        public IActionResult ResetPass(ChangePasswordRequest request)
        {
            var response = _forgotpassword.ResetPassword(request);
            return Ok(response);
        }
        private string ipAddress()
        {
            // get source ip address for the current request
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }

    }
}
