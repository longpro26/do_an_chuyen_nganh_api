﻿using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Authorization;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class StudentCompanyController : Controller
    {
        IStudentCompanyService _studentCompanyService;
        public StudentCompanyController(IStudentCompanyService studentCompanyService)
        {
            _studentCompanyService = studentCompanyService;
        }

        [HttpGet("get-list-studentcompany/{idst}")]
        public IActionResult GetListEntryByIdStudent(int idst)
        {
            var listCompany = _studentCompanyService.GetListStudentCompany(idst);
            return Ok(listCompany);
        }

        [HttpGet("get-studentcompany/{idco}")]
        public IActionResult GetStudentCompanyById(int idco)
        {
            var company = _studentCompanyService.GetStudentCompanybyId(idco);
            return Ok(company);
        }

        [HttpPut("update-studentcompany/{idco}")]
        public IActionResult UpdateStudentCompanyById(int idco ,StudentCompanyRequest model)
        {
            var result = _studentCompanyService.UpdateStudentCompanybyId(idco,model);
            return Ok(result);
        }
        [HttpPost("create-studentcompany/{idstu}")]
        public IActionResult CreateStudentCompanyById(int idstu, StudentCompanyRequest model)
        {
            var result = _studentCompanyService.CreateStudentCompany(idstu,model);
            return Ok(result);
        }

        [HttpDelete("delete-studentcompany/{idco}")]
        public IActionResult DeleteStudentCompanyById(int idco)
        {
            var result = _studentCompanyService.DeleteStudentCompanybyId(idco);
            return Ok(result);
        }
    }
}
