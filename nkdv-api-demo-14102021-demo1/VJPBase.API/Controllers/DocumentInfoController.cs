﻿using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Authorization;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class DocumentInfoController : ControllerBase
    {
        private IDocumentInfoService _documentInfoService;
        private IDocumentService _documentService;
        private IFirebaseStorageService _firebaseStorageService;
        public DocumentInfoController(IDocumentInfoService documentInfoService, IFirebaseStorageService firebaseStorageService, IDocumentService documentService)
        {
            _documentInfoService = documentInfoService;
            _documentService = documentService;
            _firebaseStorageService = firebaseStorageService;
        }
        [HttpGet("GetDocumentId/{id}")]
        public IActionResult GetDocumentsByStudentId(int Id)
        {
            var documents = _documentInfoService.GetDocumentsByStudentId(Id);
            return Ok(documents);
        }
        [HttpGet("GetDocument2Id/{id}")]
        public IActionResult GetDocumentsByStudent2Id(int Id)
        {
            var documents = _documentInfoService.GetDocument2sByStudentId(Id);
            return Ok(documents);
        }
        [HttpGet("GetDocument3Id/{id}")]
        public IActionResult GetDocumentsByStudent3Id(int Id)
        {
            var documents = _documentInfoService.GetDocument3sByStudentId(Id);
            return Ok(documents);
        }
        [HttpGet("GetCanDocument/{id}")]
        public IActionResult GetCanDocumentByStudentId(int Id)
        {
            var documents = _documentInfoService.GetCanDocumentByStudentId(Id);
            return Ok(documents);
        }
        [HttpPost("Edit")]
        public IActionResult UpdateDocument(UpdateDocumentRequest obj)
        {
            var documents = _documentInfoService.UpdateDocument(obj);
            return Ok(documents);
        }
        [HttpPost("AddDocument")]
        //public IActionResult CreateDocument(DataDocumentRequest obj)
        //{
        //    var documents = _documentInfoService.CreateDocument(obj);
        //    return Ok(documents);
        //}
        public async Task<IActionResult> CreateDocument([FromForm] UploadFileRequest request)
        {
            var fileContent = request.FileProfile.OpenReadStream();
            var extensionFile = Path.GetExtension(request.FileProfile.FileName);
            var fileName = await _firebaseStorageService.PutFileToFirebaseAsync(fileContent, extensionFile);
            DateTime fromDate = Convert.ToDateTime(request.ReceivedDate).Date;
            var obj = new DataDocumentRequest()
            {
                Id = request.Id,
                Name = request.Name,
                Receiver = request.Receiver,
                ReceivedDate = fromDate,
                Submitter = request.Submitter,
                Note = request.Note,
                StudentId = request.StudentId,
                Type = request.Type,
                FileProfile = fileName
            };
            var documents = _documentService.CreateDocument(obj);

            return Ok();
            // Store fileName to database
        }
        [HttpGet("GetTypeHS")]
        public IActionResult GetTypeHS()
        {
            var documents = _documentInfoService.GetTypeHS();
            return Ok(documents);

        }
        [HttpGet("GetTypeNKDV")]
        public IActionResult GetTypeNKDV()
        {
            var documents = _documentInfoService.GetTypeNKDV();
            return Ok(documents);

        }
        [HttpGet("GetTypeSchool")]
        public IActionResult GetTypeSchool()
        {
            var documents = _documentInfoService.GetTypeSchool();
            return Ok(documents);

        }
    }
}
