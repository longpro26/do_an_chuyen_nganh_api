﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Services;
using System;
using VJPBase.API.Authorization;
using VJPBase.API.Entities;
using VJPBase.API.Models.Requests;
using VJPBase.API.Services;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class CostOverviewController : ControllerBase
    {
        private ICostOverview _costOverview;
        public CostOverviewController(ICostOverview costOverview)
        {
            _costOverview = costOverview;
        }

        [HttpPost("costoverview")]
        public IActionResult CostOverview()
        {
            var response = _costOverview.GetAllCostovervieww();
            return Ok(response);
        }

        [HttpPost("searchname")]
        public IActionResult SearchName(CostOverviewRequest mode)
        {
            var response = _costOverview.SearchName(mode);
            return Ok(response);
        }
        [HttpPost("list-grade")]
        public IActionResult Grades()
        {
            var respone = _costOverview.Grades();
            return Ok(respone);
        }

        [HttpPost("filter-overview")]
        public IActionResult FilterOverview(FilterOverviewRequest mode)
        {
            var respone = _costOverview.FilterOverview(mode);
            return Ok(respone);
        }

        [HttpPost("delete-overview")]
        public IActionResult DeleteCostOverview(RemoverCostOverview mode)
        {
            var respone = _costOverview.RemoveCostOverview(mode);
            return Ok(respone);
        }

        [HttpPost("update-overview")]
        public IActionResult ViewID(UpdateCostOverviewRequest mode)
        {
            var respone = _costOverview.UpdateCostOverview(mode);
            return Ok(respone);
        }



    }
}
