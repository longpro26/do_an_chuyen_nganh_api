﻿using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Services;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Authorization;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UserStaffController : Controller
    {
        private IUserStaffService _userStaff;

        public UserStaffController(IUserStaffService userStaff)
        {
            _userStaff = userStaff;
        }

        [Authorize(Role.Admin)]
        [HttpPost("reregister-userstaff")]
        public IActionResult ReregisterUserStaff(RegisterUserStaffRequest request)
        {
            var cost = _userStaff.ReregisterUserStaff(request);
            return Ok(cost);
        }


        [HttpPost("list-user-admin")]
        public IActionResult ListUserAdmin()
        {
            var list= _userStaff.ListUserAdmin();
            return Ok(list );
        }


        [HttpPost("list-user-staff")]
        public IActionResult ListUserStaff()
        {
            var list = _userStaff.ListUserStaff();
            return Ok(list);
        }



        [HttpPost("list-user-teacher")]
        public IActionResult ListUserTeacher()
        {
            var list = _userStaff.ListUserTeacher();
            return Ok(list);
        }

        [AllowAnonymous]
        [HttpPost("count-userstaff")]
        public IActionResult CountUserStaff()
        {
            var list = _userStaff.CoutUserStaff();
            return Ok(list);
        }

        [AllowAnonymous]
        [HttpPost("count-Teacher")]
        public IActionResult CountTeacher()
        {
            var list = _userStaff.CoutTeacher();
            return Ok(list);
        }
    }
}
