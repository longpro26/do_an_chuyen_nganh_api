﻿using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Authorization;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class WorkExperienceController : Controller
    {
        private IWorkExperiencesService _experiencesService;
        public WorkExperienceController(IWorkExperiencesService experiencesService)
        {
            _experiencesService = experiencesService;
        }

        [HttpGet("get-list-workexperience/{idst}")]
        public IActionResult GetListExperienceByIdStudent(int idst)
        {
            var listExp = _experiencesService.GetListWorkExperience(idst);
            return Ok(listExp);
        }

        [HttpGet("get-workexperience/{idexp}")]
        public IActionResult GetExperienceByIdStudent(int idexp)
        {
            var exp = _experiencesService.GetWorkExperiencebyId(idexp);
            return Ok(exp);
        }

        [HttpPost("create-workexperience/{stuid}")]
        public IActionResult CreateExperience(int stuid, WorkExperienceRequest model)
        {
            var result = _experiencesService.CreateWorkExperience(stuid, model);
            return Ok(result);
        }

        [HttpPut("update-workexperience/{idexp}")]
        public IActionResult GetExperienceByIdStudent(int idexp, WorkExperienceRequest model)
        {
            var result = _experiencesService.UpdateWorkExperiencebyId(idexp,model);
            return Ok(result);
        }

        [HttpDelete("delete-workexperience/{idexp}")]
        public IActionResult DeleteExperienceById(int idexp)
        {
            var result = _experiencesService.DeleteWorkExperiencebyId(idexp);
            return Ok(result);
        }
    }
}
