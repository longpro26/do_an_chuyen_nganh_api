﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Authorization;
using VJPBase.API.Entities;
using VJPBase.API.Models.Requests;
using VJPBase.API.Services;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class StudentController : ControllerBase
    {
        private IStudentService _studentService;
        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        [HttpGet("get-tuition")]
        public IActionResult GetTuitionOfStudent()
        {
            var students = _studentService.GetTuitionOfStudent();
            return Ok(students);
        }

        [HttpGet("get-all-student")]
        public IActionResult GetAllStudent()
        {
            var students = _studentService.GetAllStudentTotal();
            return Ok(students);
        }

        [HttpGet("get-tuition1")]
        public IActionResult GetTuitionOfStudent1()
        {
            var students = _studentService.GetTuitionOfStudent1();
            return Ok(students);
        }

        [HttpGet("get-tuition2")]
        public IActionResult GetTuitionOfStudent2()
        {
            var students = _studentService.GetTuitionOfStudent2();
            return Ok(students);
        }

        [HttpPost("find-student")]
        public IActionResult FindStudentName(FindStuOfNameRequest name)
        {
            var students = _studentService.SearchName(name);
            return Ok(students);
        }
        [HttpGet("get-asdasd")]
        public IActionResult GetCostStudent()
        {
            var student = _studentService.GetCostStudent();
            return Ok(student);
        }
       
        [HttpPost("remove-student")]
        public IActionResult RemoveStudent(RemoveStudentRequest remove)
        {
            var students = _studentService.RemoveStudent(remove);
            return Ok(students);
        }
        [HttpGet("get-tuition-debt")]
        public IActionResult GetTuitionDebtStudent()
        {
            var student = _studentService.GetTuitionDebtStudent();
            return Ok(student);
        }
        [HttpPost("find-student-1")]
        public IActionResult FindStudentName1(FindStuOfNameRequest name)
        {
            var students = _studentService.SearchNameByTuition1(name);
            return Ok(students);
        }

        [HttpPost("find-student-2")]
        public IActionResult FindStudentName2(FindStuOfNameRequest name)
        {
            var students = _studentService.SearchNameByTuition2(name);
            return Ok(students);
        }

        [AllowAnonymous]
        [HttpPost("change-password")]
        public IActionResult ChangePassword(ChangePassStudentRequest request)
        {
            var students = _studentService.ChangePassStudent(request);
            return Ok(students);
        }
        [AllowAnonymous]
        [HttpPost("count-student")]
        public IActionResult CoutStudent()
        {
            var students = _studentService.CountStudent();
            return Ok(students);
        }
    }
}
