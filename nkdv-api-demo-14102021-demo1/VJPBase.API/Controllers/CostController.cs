﻿using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Models.Response.Dto;
using NKVD.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Authorization;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class CostController : Controller
    {
        private ICostService _costService;

        public CostController(ICostService costService)
        {
            _costService = costService;
        }

        [HttpGet("get_cost/{costId}")]
        public IActionResult GetCost(int costId)
        {
            var cost = _costService.GetCostbyId(costId);
            return Ok(cost);
        }

        [HttpPost("create_cost")]
        public IActionResult Create(CostRequest model)
        {
            var result = _costService.InsertCost(model);
            return Ok(true);
        }

        [HttpPost("createorupdate_cost/{id}")]
        public IActionResult CreateOrUpdate(int id, CostRequest model)
        {
            if (_costService.CheckUpdate(id))
            {
                var cost = _costService.UpdateCost(id, model);
            }
            else
            {
                var result = _costService.InsertCost(model);
            }
            return Ok(true);
        }

        [HttpPut("update_cost/{id}")]
        public IActionResult UpdateCost(int id, CostRequest model)
        {
            var cost = _costService.UpdateCost(id, model);
            return Ok(cost);
        }
        [HttpGet("Type")]
        public IActionResult TypeCost()
        {
            var lst = _costService.GetTypeCost();
            return Ok(lst);
        }
    }
}
