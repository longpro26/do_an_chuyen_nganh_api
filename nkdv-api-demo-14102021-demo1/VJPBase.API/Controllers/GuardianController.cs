﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NKVD.API.Models.Requests;
using NKVD.API.Services;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Authorization;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class GuardianController : ControllerBase
    {
        private IGuardianService _guardian;
    

        public GuardianController(IGuardianService guardianlist)
        {
            _guardian = guardianlist;
          
        }   
        [HttpGet("get_guardianid/{id}")]
        public IActionResult GetGuardianById(int id)
        {
            var result = _guardian.GetByIdGuardian(id);
            return Ok(result);
        }
        [Authorize(Role.Admin)]
        [HttpPost("createorUpdate_guardian")]
        public IActionResult CreateGuardian(GuardianCreateRequest model)
        {

            var guardian = _guardian.CreateOrUpdateGuardianInfo(model);
            return Ok(guardian);
        }

        [Authorize(Role.Admin)]
        [HttpPut("update-guardianinfo")]
        public IActionResult UpdateGuardian(GuardianCreateRequest model)
        {
            var result = _guardian.UpdateGuardianId(model);
            return Ok(result);
        }

    }
}
