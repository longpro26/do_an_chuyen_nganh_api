﻿using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Authorization;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class EntryHistoryController : Controller
    {
        private IEntryHistoryService _entryService;
        public EntryHistoryController(IEntryHistoryService entryService)
        {
            _entryService = entryService;
        }

        [HttpGet("get-list-entryhistory/{idst}")]
        public IActionResult GetListEntryByIdStudent(int idst)
        {
            var listEntry = _entryService.GetListEntryHistory(idst);
            return Ok(listEntry);
        }

        [HttpDelete("delete-entryhistory/{entryId}")]
        public IActionResult DeleteEntry(int entryId)
        {
            var result = _entryService.DeleteEntrybyId(entryId);
            return Ok(result);
        }

        [HttpPut("update-entryhistory/{entryId}")]
        public IActionResult UpdateEntry(int entryId,EntryHistoryRequest entry)
        {
            var result = _entryService.UpdateEntrybyId(entryId,entry);
            return Ok(result);
        }

        [HttpGet("get-entryhistory/{entryId}")]
        public IActionResult GetEntry(int entryId)
        {
            var result = _entryService.GetEntrybyId(entryId);
            return Ok(result);
        }

        [HttpPost("create-entryhistory/{idstu}")]
        public IActionResult CreateEntry(int idstu,EntryHistoryRequest entry)
        {
            var result = _entryService.CreateEntry(idstu,entry);
            return Ok(result);
        }
    }
}
