﻿using Microsoft.AspNetCore.Mvc;
using NKVD.API.Models.Requests;
using NKVD.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Authorization;

namespace NKVD.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class DetailedCostsController : Controller
    {
        private IDetailedCostService _detailedcostService;

        public DetailedCostsController(IDetailedCostService detailedcostService)
        {
            _detailedcostService = detailedcostService;
        }

        [HttpGet("get_all_detailedcost/{studentId}")]
        public IActionResult GetAllDetailedCost(int studentId)
        {
            var detailedcost = _detailedcostService.GetAllDetailedCost(studentId);
            return Ok(detailedcost);
        }

        [HttpGet("get_by_id/{detailedcostdId}")]
        public IActionResult GetStudentById(int detailedcostdId)
        {
            var detailedcost = _detailedcostService.GetDetailedCostbyId(detailedcostdId);
            return Ok(detailedcost);
        }


        [HttpDelete("delete_detailedcost/{detailedcostdId}")]
        public IActionResult DeleteDetailedCost(int detailedcostdId)
        {
            var result = _detailedcostService.DeleteDetailedCost(detailedcostdId);
            return Ok(result);
        }

        [HttpPut("update_detailedcost/{id}")]
        public IActionResult UpdateDetailedCost(int id, DetailedCostsRequest model)
        {
            var result = _detailedcostService.UpdateDetailedCost(id, model);
            return Ok(result);
        }

        [HttpPost("create_detailedcost")]
        public IActionResult Create(DetailedCostsRequest model)
        {
            var result = _detailedcostService.InsertDetailedCost(model);
            return Ok(true);
        }
    }
}
