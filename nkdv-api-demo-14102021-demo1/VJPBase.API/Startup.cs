﻿using BCryptNet = BCrypt.Net.BCrypt;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using VJPBase.API.Authorization;
using VJPBase.API.Middlewares;
using VJPBase.API.Entities;
using VJPBase.API.Helpers;
using VJPBase.API.Services;
using VJPBase.API.Services.Impl;
using System.Collections.Generic;
using NKVD.Entities.Context;
using Microsoft.EntityFrameworkCore;
using NKVD.API.Services;
using NKVD.API.Services.Impl;
using NKVD.API.Models.Response.Dto;

namespace VJPBase.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //config mail
            services.AddOptions();                                         // Kích hoạt Options
            var mailsettings = Configuration.GetSection("MailSettings");  // đọc config
            services.Configure<MailSettings>(mailsettings);                // đăng ký để Inject
            // Đăng ký SendMailService với kiểu Transient, mỗi lần gọi dịch
             // vụ ISendMailService một đới tượng SendMailService tạo ra (đã inject config)


            services.AddDbContext<DataContext>();

            var connectionString = Configuration.GetConnectionString("NKDVConnection");
            //services.AddDbContext<NKDVContext>(op => op.UseSqlServer(connectionString, b => b.MigrationsAssembly("NKVD.Entities")));
            services.AddDbContext<NKDVDbContext>(op => op.UseSqlServer(connectionString));

            services.AddCors();
            services.AddControllers().AddJsonOptions(x => x.JsonSerializerOptions.IgnoreNullValues = true);
            services.AddHttpContextAccessor();
            //services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // configure strongly typed settings object
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            // configure DI for application services
            services.AddScoped<IJwtUtils, JwtUtils>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IFirebaseStorageService, FirebaseStorageService>();
            services.AddScoped<ICostOverview, CostOverview>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IStudentService, StudentService>();
            services.AddScoped<IEntryHistoryService, EntryHistoryService>();
            services.AddScoped<IWorkExperiencesService, WorkExperienceService>();
            services.AddScoped<IStudentCompanyService, StudentCompanyService>();
            services.AddScoped<IEduBackgroundService, EduBackgroundService>();
            services.AddScoped<IFamilyInformation, FamilyInformation>();
            services.AddScoped<ICostService, CostService>();
            services.AddScoped<IDetailedCostService, DetailedCostsService>();
            services.AddScoped<IDocumentService, DocumentService>();
            services.AddScoped<IDocumentInfoService, DocumentInfoService>();
            services.AddScoped<IGuardianService,GuardianService>();
            services.AddScoped<ISendMailService, SendMailService>();
            services.AddScoped<IForgotPassword, ForgotPassword>(); 
            services.AddScoped<IUserStaffService, UserStaffService>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "VJPBase.API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DataContext context)
        {
            createTestUser(context);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "VJPBase.API v1"));
            }

            app.UseCors(x => x
                .SetIsOriginAllowed(origin => true)
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseMiddleware<ErrorHandlerMiddleware>();

            // custom jwt auth middleware
            app.UseMiddleware<JwtMiddleware>();
            app.UseMiddleware<ErrorHandlerMiddleware>();

            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void createTestUser(DataContext context)
        {
            // add hardcoded test user to db on startup
            var testUsers = new List<User>
            {
                new User { Id = 1, FirstName = "Admin", LastName = "User", Username = "admin", PasswordHash = BCryptNet.HashPassword("admin"), Role = Role.Admin },
                new User { Id = 2, FirstName = "Normal", LastName = "User", Username = "nkdv@nkdv.com", PasswordHash = BCryptNet.HashPassword("nkdv1234@2021"), Role = Role.User }
            };
            context.Users.AddRange(testUsers);
            context.SaveChanges();
        }
    }
}
