﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class WorkExperienceResponse
    {
        public int Id { get; set; }
        public string WorkName { get; set; }
        public string Address { get; set; }
        public string Position { get; set; }
        public string Form { get; set; }
        public string To { get; set; }
    }
}
