﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class EduBackgroundRespone
    {
        public int Id { get; set; }
        public string SchoolName { get; set; }
        public string Form { get; set; }
        public string To { get; set; }
        public string Address { get; set; }
    }
}
