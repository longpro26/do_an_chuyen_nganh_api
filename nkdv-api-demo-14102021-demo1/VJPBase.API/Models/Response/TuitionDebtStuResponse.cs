﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class TuitionDebtStuResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string FacebookUrl { get; set; }
        public string CurrentAddress { get; set; }
        public double Price { get; set; }
    }
}
