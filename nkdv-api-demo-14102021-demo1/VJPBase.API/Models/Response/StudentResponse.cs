﻿using NKVD.API.Models.Response.Dto;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using VJPBase.API.Entities;
using VJPBase.API.Models.Response.Dto;

namespace VJPBase.API.Models.Response
{
    public class StudentResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string FacebookUrl { get; set; }
        public string CurrentAddress { get; set; }
        public double Price { get; set; }
        public string BirthDate { get; set; }
        public List<DetailedCost> detailedCosts { get; set; }
    }
}    

