﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class CostOverview2Response
    {
        public int Stt { get; set; }
        public string DateOfFiling { get; set; }
        public string Grade { get; set; }
        public string Submitter { get; set; }
        public string NameStudent { get; set; }
        public string Collector { get; set; }
        public List<Models.Response.Dto.TypeNameDto> Type { get; set; }
        public double Price { get; set; }
        public string Note { get; set; }
        public int CostOfStudentId { get; set; }
    }
}
