﻿using NKVD.API.Models.Response.Dto;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class GuardianResponse
    {
        //guardian
        public string Id { get; set; }
        public string WorkPlace { get; set; }
        public string PhoneNumber { get; set; }
        public string RepresentativePosition { get; set; }
        public DateTime WorkingTime { get; set; }
        public string GuardianPosition { get; set; }
        public string TaxCode { get; set; }
        //company
        public string NameCompany { get; set; }
        public string BusinessProfession { get; set; }
        public string TaxCodeCompany { get; set; }
        public string PhoneNumberCompany { get; set; }
        public string Address { get; set; }
        public string Representative { get; set; }
        //indentification
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string IssuedBy { get; set; }
        // 0: Female; 1: Male
        public byte Sex { get; set; }
        public string PermanentAddress { get; set; }
        public string CurrentAddress { get; set; }
        public string HometownAddress { get; set; }
        public DateTime Birthday { get; set; }
        public string BirthPlace { get; set; }

 

    }
}
