﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class CanDocumentResponse
    {
        public int? Id { get; set; }
        public List<string> Type { get; set; }
    }
}
