﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace NKVD.API.Models.Response.Dto
{
    public class GuardianInfor
    {
        public string WorkPlace { get; set; }
        public string PhoneNumber { get; set; }
        public string RepresentativePosition { get; set; }
        public DateTime WorkingTime { get; set; }
        public string GuardianPosition { get; set; }
        public int TaxCode { get; set; }



    }
}
