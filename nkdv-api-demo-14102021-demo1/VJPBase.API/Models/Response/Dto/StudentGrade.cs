﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class StudentGrade
    {
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public int GradeId { get; set; }
        public Grade Grade { get; set; }
    }
}
