﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class FileDto
    {
        public MemoryStream Content { get; set; }

        public string ContentType { get; set; }
    }
}
