﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class TypeCostRespon
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
    }
}
