﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class DocumentFillDataDto
    {
        public string Document { get; set; }
        public string FileName { get; set; }
    }
}
