﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class TypeNameDto
    {
        public int IdCostDetail { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
    }
}
