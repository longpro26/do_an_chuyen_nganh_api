﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class DetailedCost : BaseEntity
    {
        public string Submitter { get; set; }
        public string Collector { get; set; }
        public DateTime DateOfLing { get; set; }
        public string Note { get; set; }
        //foreign key
        public int CostId { get; set; }
        public Cost Cost { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
    }
}
