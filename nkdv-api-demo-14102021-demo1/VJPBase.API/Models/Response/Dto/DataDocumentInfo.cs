﻿using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class DataDocumentInfo
    {
        public int Id { get; set; }
        public string ClassMate { get; set; }
        public string StudentName { get; set; }
        public string Receiver { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string Note { get; set; }
        public string Type { get; set; }
        public int? StudentId { get; set; }
    }
}
