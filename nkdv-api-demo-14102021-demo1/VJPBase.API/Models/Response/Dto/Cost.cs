﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class Cost : BaseEntity
    {
        public double Price { get; set; }
        public string Type { get; set; }
    }
}
