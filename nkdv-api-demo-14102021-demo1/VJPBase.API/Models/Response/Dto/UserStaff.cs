﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class UserStaff
    {
        public  int UserStaffID { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
        public string Password{ get; set; }
    }
}
