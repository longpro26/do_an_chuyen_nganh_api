﻿using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class StudentInfo
    {
        public string PhoneNumber { get; set; }
        public string FacebookUrl { get; set; }
        public int IdentificationCardOfStudentId { get; set; }       
    }
}
