﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class Grade
    {
        public string Name { get; set; }
        public DateTime OpenDate { get; set; }
        public IList<StudentGrade> StudentGrades { get; set; }
        public IList<TeacherGrade> TeacherGrades { get; set; }
    }
}
