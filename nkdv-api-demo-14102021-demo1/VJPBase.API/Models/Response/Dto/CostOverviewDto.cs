﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class CostOverviewDto
    {
        public int Stt { get; set; }
        public DateTime DateOfFiling {get; set; }
        public string Grade { get; set; }
        public string NameStudent { get; set; }
        public string Collector { get; set; }
        public string Type { get; set; }
        public double Price { get; set; }
        public string Note { get; set; }
    }
}
