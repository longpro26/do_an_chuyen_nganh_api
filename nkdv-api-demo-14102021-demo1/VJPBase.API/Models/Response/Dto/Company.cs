﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class Company : BaseEntity
    {
        public string Name { get; set; }
        public string BusinessProfession { get; set; }
        public string TaxCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string Representative { get; set; }
    }
}
