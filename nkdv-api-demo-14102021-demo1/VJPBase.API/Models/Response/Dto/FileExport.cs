﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class FileExport
    {
        public MemoryStream MemoryStream { get; set; }
        public string FileName { get; set; }
    }
}
