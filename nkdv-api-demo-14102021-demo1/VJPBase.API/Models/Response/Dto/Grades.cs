﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class Grades
    {
        public int id { get; set; }
        public string Name { get; set; }
        public DateTime? OpenDate { get; set; }
    }
}
