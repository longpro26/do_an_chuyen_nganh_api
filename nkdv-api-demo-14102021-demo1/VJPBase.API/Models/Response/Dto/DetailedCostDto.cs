﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class DetailedCostDto
    {
        public int Id { get; set; }
        public DateTime DateOfLing { get; set; }
        public string Submitter { get; set; }
        public string Collector { get; set; }
        public string Type { get; set; }
        public double Price { get; set; }
        public string Note { get; set; }
    }
}
