﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class DetailedCostInfo
    {
        public string Submitter { get; set; }
        public string Collector { get; set; }
        public DateTime DateOfLing { get; set; }
        public string Type { get; set; }
        public double Piece { get; set; }
        public string Note { get; set; }
    }
}
