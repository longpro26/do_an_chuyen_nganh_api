﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response.Dto
{
    public class CostInfo
    {
        public double TotalPrice { get; set; }
        public virtual int TotalCostOfStudentId { get; set; }
        public DateTime ContractSigningDate { get; set; }
        public string Note { get; set; }
    }
}
