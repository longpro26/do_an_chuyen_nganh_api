﻿using NKVD.API.Models.Response.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class DetailedCostsResponse
    {
        public int Id { get; set; }
        public string Submitter { get; set; }
        public string Collector { get; set; }
        public DateTime DateOfLing { get; set; }
        public List<Models.Response.Dto.TypeNameDto> Type { get; set; }
        public double Piece { get; set; }
        public string Note { get; set; }
        //foreign key
        public virtual int CostOfStudentId { get; set; }

        public string NameStudent { get; set; }
    }
}
