﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class StudentInfoResponse
    {
        public int STT { get; set; }
        public int StudentId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string CurrentAddress { get; set; }
        public string HometownAddress { get; set; }
        public double TotalPrice { get; set; }
        public virtual int TotalCostOfStudentId { get; set; }
        public double Piece { get; set; }
        public string FacebookUrl { get; set; }
        public string BirthDate { get; set; }
    }
}
