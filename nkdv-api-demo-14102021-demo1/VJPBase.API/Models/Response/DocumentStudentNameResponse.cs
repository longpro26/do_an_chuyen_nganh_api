﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class DocumentStudentNameResponse
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
    }
}
