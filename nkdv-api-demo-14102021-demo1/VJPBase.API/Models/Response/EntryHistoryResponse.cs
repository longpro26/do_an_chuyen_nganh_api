﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class EntryHistoryResponse
    {
        public int Id { get; set; }
        public String EntryDate { get; set; }
        public String OutputDate { get; set; }
        public string PassportValidity { get; set; }
        public string Purpose { get; set; }
    }
}
