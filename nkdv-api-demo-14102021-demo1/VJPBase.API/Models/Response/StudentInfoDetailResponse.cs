﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class StudentInfoDetailResponse
    {
        public int Id { get; set; }
        //table student
        public string PhoneNumber { get; set; }
        public string FacebookUrl { get; set; }
        public string Gmail { get; set; }
        public int IdentificationCardOfStudentId { get; set; }

        //table IdentificationCard
        public string IdIdentificationCard { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public string IssuedBy { get; set; }
        // 0: Female; 1: Male
        public byte Sex { get; set; }
        public string PermanentAddress { get; set; }
        public string CurrentAddress { get; set; }
        public string HometownAddress { get; set; }
        public string Birthday { get; set; }
        public string BirthPlace { get; set; }
        //table visa
        public string? DateRange { get; set; }
        public string? ExpirationDate { get; set; }
        public string? IssuedByVisa { get; set; }
        public string? Type { get; set; }

        //table grade
        public List<String>? NameGrades { get; set; }

        //table infoinjapans
        public string? Address { get; set; }
        public string? AreaCode { get; set; }
        public string? AddressType { get; set; }
        public string? Majors { get; set; }
        public string? Specialized { get; set; }
        public string? SchoolName { get; set; }
    }
}
