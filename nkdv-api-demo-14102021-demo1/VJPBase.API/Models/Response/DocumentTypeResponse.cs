﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class DocumentTypeResponse
    {
        public string type { get; set; }
    }
}
