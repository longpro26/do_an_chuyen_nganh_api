﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Response
{
    public class CostResponse
    {
        public int Id { get; set; }
        public double TotalPrice { get; set; }
        public DateTime ContractSigningDate { get; set; }
        public string Note { get; set; }
        public virtual int TotalCostOfStudentId { get; set; }
    }
}
