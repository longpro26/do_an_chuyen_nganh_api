﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class ForgotPasswordEmailRequest
    {
        [Required]
        public string email{ get; set; }
    }
}
