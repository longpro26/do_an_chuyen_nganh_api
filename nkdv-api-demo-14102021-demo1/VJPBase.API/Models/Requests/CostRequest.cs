﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class CostRequest
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public double TotalPrice { get; set; }
        [Required]
        public string ContractSigningDate { get; set; }
       
        public string Note { get; set; }
        public virtual int TotalCostOfStudentId { get; set; }
    }
}
