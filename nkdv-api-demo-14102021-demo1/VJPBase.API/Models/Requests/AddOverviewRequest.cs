﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class AddOverviewRequest
    {
        [Required]
        public int Stt { get; set; }
        [Required]
        public DateTime DateOfFiling { get; set; }
        [Required]
        public string Grade { get; set; }
        [Required]
        public string NameStudent { get; set; }
        [Required]
        public string Collector { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public double Price { get; set; }
        public string Note { get; set; }
    }
}
