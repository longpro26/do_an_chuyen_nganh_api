﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class StudentCompanyRequest
    {       
        [Required]
        public string Name { get; set; }
        [Required]
        public string BusinessProfession { get; set; }
        [Required]
        public string TaxCode { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Representative { get; set; }
    }
}
