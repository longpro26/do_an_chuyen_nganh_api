﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class EntryHistoryRequest
    {
        [Required]
        public string EntryDate { get; set; }
        [Required]
        public string OutputDate { get; set; }
        [Required]
        public string PassportValidity { get; set; }
        [Required]
        public string Purpose { get; set; }
    }
}
