﻿using Microsoft.AspNetCore.Http;
using NKVD.API.Models.Response.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class DataDocumentRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FileProfile { get; set; }
        public string Submitter { get; set; }
        public string Receiver { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string Note { get; set; }
        public string Type { get; set; }
        public string StudentName { get; set; }

        public int StudentId { get; set; }
    }
}
