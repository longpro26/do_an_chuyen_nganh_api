﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class UpdateCostOverviewRequest
    {
        [Required]
        public int Id{ get; set; }
        [Required]
        public string DateOfFiling { get; set; }
        public string Submitter { get; set; }
        [Required]
        public string NameStudent { get; set; }
        [Required]
        public string Collector { get; set; }
        [Required]
        public List<int> Type { get; set; }
        [Required]
        public double Price { get; set; }
        public string Note { get; set; }
    }
}
