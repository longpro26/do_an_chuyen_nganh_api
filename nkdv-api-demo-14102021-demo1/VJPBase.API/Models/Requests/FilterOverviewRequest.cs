﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class FilterOverviewRequest
    {
        public string StarDate { get; set; }
        public string EndDate { get; set; }
        public string Grade { get; set; }
        public string Type { get; set; }
    }
}
