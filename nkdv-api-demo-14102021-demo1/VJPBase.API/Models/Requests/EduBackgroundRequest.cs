﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class EduBackgroundRequest
    {
        [Required]
        public string SchoolName { get; set; }
        [Required]
        public string Form { get; set; }
        [Required]
        public string To { get; set; }
        [Required]
        public string Address { get; set; }
    }
}
