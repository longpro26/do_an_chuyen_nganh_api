﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class UpdateFamilyInformationRequest
    {
        [Required]
        public int Stt { get; set; }
        [Required]
        public string Relationship { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string BirthDate { get; set; }
        public string PhoneNumber { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Job { get; set; }
    }
}
