﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class IdCostOverviewRequest
    {
        public int Id { get; set; }
    }
}
