﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class ChangePassStudentRequest
    {
        [Required]
        public string Username{ get; set; }
        [Required]
        public string Passwordcu{ get; set; }
        [Required]
        public string PasswordNew{ get; set; }
    }
}
