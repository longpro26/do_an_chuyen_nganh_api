﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class DetailedCostsRequest
    {
        [Required]
        public int Id { get; set; }
      
        public string Submitter { get; set; }
        [Required]
        public string Collector { get; set; }
        [Required]
        public string DateOfLing { get; set; }
        [Required]
        public List<int> Type { get; set; }
        [Required]
        public double Piece { get; set; }
        
        public string Note { get; set; }
        [Required]
        public virtual int CostOfStudentId { get; set; }
    }
}
