﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class GuardianCreateRequest
    {
        //guardian
        public int Id { get; set; }
        [Required]
        public string WorkPlace { get; set; }
        [Required]
        public string PhoneNumberGuardian { get; set; }
        [Required]
        public string RepresentativePosition { get; set; }
        [Required]
        public string WorkingTime { get; set; }
        [Required]
        public string GuardianPosition { get; set; }
        [Required]
        public string TaxCodeGuardian { get; set; }
        //student
        [Required]
        public int StudentId { get; set; }

        //indentificationcard
        [Required]
        public string IdIdentificationCard { get; set; }
        [Required]
        public string IdenttificationCardName { get; set; }
        [Required]
        public string Date { get; set; }
        [Required]
        public string IssuedBy { get; set; }
        [Required]
        public byte Sex { get; set; }   
        [Required]
        public string PermanentAddress { get; set; }
        [Required]
        public string CurrentAddress { get; set; }
        [Required]
        public string HometownAddress { get; set; }
        [Required]
        public string Birthday { get; set; }
        [Required]
        public string BirthPlace { get; set; }
        //Company
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public string BusinessProfession { get; set; }
        [Required]
        public string TaxCodeCompany { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string PhoneNumberCompany { get; set; }
        [Required]
        public string Representative { get; set; }
      
        
    }
}
