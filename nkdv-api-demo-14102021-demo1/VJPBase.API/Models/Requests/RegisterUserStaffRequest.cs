﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class RegisterUserStaffRequest
    {
        [Required]
        public string FullName { get; set; }
        [Required]
        public DateTime Dateofbirth { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public int Role { get; set; }
        [Required]
        public string Password{ get; set; }
        [Required]
        public string Email { get; set; }
    }
}
