﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class FilterDocumentRequest
    {
        public string StartDay { get; set; }
        public string EndDay { get; set; }
        public string Receiver { get; set; }
        public string TypeDocument { get; set; }
    }
}
