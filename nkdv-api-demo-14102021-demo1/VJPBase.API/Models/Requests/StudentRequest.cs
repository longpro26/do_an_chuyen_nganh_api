﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VJPBase.API.Models.Requests
{
    public class StudentRequest
    {
        public string PhoneNumber { get; set; }    
        public string Gmail { get; set; }
        public string FacebookUrl { get; set; }
        [Required]
        public int IdentificationCardOfStudentId { get; set; }
        [Required]
        public string IdIdentificationCard { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Date { get; set; }
        [Required]
        public string IssuedBy { get; set; }
        // 0: Female; 1: Male
        [Required]
        public byte Sex { get; set; }
        [Required]
        public string PermanentAddress { get; set; }
        [Required]
        public string CurrentAddress { get; set; }
        [Required]
        public string HometownAddress { get; set; }
        [Required]
        public string Birthday { get; set; }
        [Required]
        public string BirthPlace { get; set; }
        //table visa
        
        public string DateRange { get; set; }
        
        public string ExpirationDate { get; set; }
        
        public string IssuedByVisa { get; set; }
       
        public string Type { get; set; }
        //table infoinjapans
        
        public string Address { get; set; }
        
        public string AreaCode { get; set; }
       
        public string AddressType { get; set; }
        
        public string Majors { get; set; }
        
        public string Specialized { get; set; }
        
        public string SchoolName { get; set; }
    }
}
