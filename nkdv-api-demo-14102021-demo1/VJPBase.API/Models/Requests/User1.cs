﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class User1
    {
        public int? StudentID { get; set; }
        public int? AdminID { get; set; }
        public int? TeacherID { get; set; }
        public int? UserStaffID { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public int Role { get; set; }

        [JsonIgnore]
        public string PasswordHash { get; set; }

 
    }
}
