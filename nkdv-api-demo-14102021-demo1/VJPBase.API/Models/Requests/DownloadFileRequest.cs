﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Models.Requests
{
    public class DownloadFileRequest
    {
        public string Url { get; set; }
        public string FileName { get; set; }
    }
}
