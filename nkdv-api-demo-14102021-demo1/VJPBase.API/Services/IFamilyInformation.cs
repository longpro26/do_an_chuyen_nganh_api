﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.API.Models.Response.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface IFamilyInformation
    {
        
        List<FamilyInformationResponse> Familyinformationn(int StudentId);
        bool DeleteFamilyinformationn(int Id);
        bool UpdateFamilyinformationn(UpdateFamilyInformationRequest mode);
        bool CreateFamilyinformationn(int studentId, AddFamilyInformationRequest mode);


    }
}
