﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface IDocumentInfoService
    {
        IEnumerable<DocumentResponse> GetDocumentsByStudentId(int Id);
        IEnumerable<DocumentResponse> GetDocument2sByStudentId(int Id);
        IEnumerable<DocumentResponse> GetDocument3sByStudentId(int Id);
        IEnumerable<CanDocumentResponse> GetCanDocumentByStudentId(int Id);
        IEnumerable<DocumentTypeResponse> GetTypeHS();
        IEnumerable<DocumentTypeResponse> GetTypeNKDV();
        IEnumerable<DocumentTypeResponse> GetTypeSchool();
        bool UpdateDocument(UpdateDocumentRequest data);
        bool CreateDocument(DataDocumentRequest data);
    }
}
