﻿using NKVD.API.Models.Response.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface IFirebaseStorageService
    {
        Task<string> PutFileToFirebaseAsync(Stream stream, string extensionFileName);

        Task<FileDto> DownloadFileFromFireBaseAsync(string fileName);
    }
}
