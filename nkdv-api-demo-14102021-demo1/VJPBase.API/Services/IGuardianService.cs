﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface IGuardianService
    {
        bool CreateOrUpdateGuardianInfo(GuardianCreateRequest model);
        GuardianResponse GetByIdGuardian(int id);
        bool UpdateGuardianId(GuardianCreateRequest model);
    }
}
