﻿using NKVD.API.Models.Response.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface ISendMailService
    {
        Task<string> SendMail(MailContent mailContent);

        Task SendEmailAsync(string email, string subject, string htmlMessage);
       public string createEmailBody(string email,string token);
    }
}
