﻿
using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.API.Models.Response.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Models.Response;

namespace NKVD.API.Services
{
    public interface IDocumentService
    {
        IEnumerable<DocumentResponse> GetAll();
        IEnumerable<DocumentResponse> GetDocumentsByStudentName(StudentNameSearchRequest name);
        IEnumerable<DocumentResponse> FilterDocument(FilterDocumentRequest data);
        IEnumerable<DocumentResponse> DeleteDocument(DocumentRequest value);
        bool UpdateDocument(DataDocumentRequest data);
        bool CreateDocument(DataDocumentRequest data);
        IEnumerable<DocumentReceiverResponse> GetReceiver();
        IEnumerable<DocumentStudentNameResponse> GetStudentName();
        IEnumerable<DocumentTypeResponse> GetType();
        FileExport GetWordDocumentFromTemplateWithTempFile(int Id,TypeExportRequets typeExport);
    }
}
