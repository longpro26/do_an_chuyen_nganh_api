﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface IUserStaffService 
    {
        public bool ReregisterUserStaff(RegisterUserStaffRequest request);
        public List<UserManagementResponse> ListUserAdmin();
        public List<UserManagementResponse> ListUserStaff();
        public List<UserManagementResponse> ListUserTeacher();
        public int CoutUserStaff();
        public int CoutTeacher();
    }
}
