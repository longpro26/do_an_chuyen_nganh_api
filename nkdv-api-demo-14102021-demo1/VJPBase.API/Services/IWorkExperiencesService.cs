﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface IWorkExperiencesService
    {
        IEnumerable<WorkExperienceResponse> GetListWorkExperience(int studentId);

        Boolean CreateWorkExperience(int stuid, WorkExperienceRequest model);

        Boolean DeleteWorkExperiencebyId(int expid);

        Boolean UpdateWorkExperiencebyId(int id, WorkExperienceRequest model);

        WorkExperienceResponse GetWorkExperiencebyId(int expid);
    }
}
