﻿using Microsoft.EntityFrameworkCore;
using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;

namespace NKVD.API.Services.Impl
{
    public class DocumentInfoService : IDocumentInfoService
    {
        private NKDVDbContext _context;
        public DocumentInfoService(NKDVDbContext context)
        {
            _context = context;
        }
        private bool CheckTypeHS(string type, IEnumerable<DocumentTypeResponse> documentTypes)
        {
            var check = documentTypes.Where(p => p.type.ToUpper() == type).Count();
            if (check > 0)
                return true;

            return false;
        }
        public IEnumerable<DocumentResponse> GetDocumentsByStudentId(int Id)
        {
            var documents = _context.Documents.Include(doc => doc.Student)
                .ThenInclude(stu => stu.IdentificationCard)
                .Include(doc => doc.Student).Where(doc => doc.StudentId == Id).ToList();
            //var documents = _context.Documents.Where(std => std.StudentId == 3).ToList()/*;*/
            var documentTypes = GetTypeHS();
            List<DocumentResponse> lstdocuments = new List<DocumentResponse>();
            documents.ForEach(p =>
            {
                DocumentResponse document = new DocumentResponse();
                document.Id = p.Id;
                document.Name = p.Name;
                document.Receiver = p.Receiver;
                document.ReceivedDate = p.ReceivedDate.ToString("yyyy/MM/dd");
                document.Submitter = p.Submitter;
                document.StudentName = p.Student.IdentificationCard.Name;
                document.Type = p.Type.ToUpper();
                document.Note = p.Note;
                document.Link = p.Link;
                document.StudentId = p.StudentId;
                lstdocuments.Add(document);
            });
            if (lstdocuments == null) throw new KeyNotFoundException("Document not found");
            IEnumerable<DocumentResponse> result = lstdocuments.Where(p => CheckTypeHS(p.Type, documentTypes)); 
            return result;
        }
        public IEnumerable<DocumentResponse> GetDocument2sByStudentId(int Id)
        {
            var documents = _context.Documents.Include(doc => doc.Student)
                .ThenInclude(stu => stu.IdentificationCard)
                .Include(doc => doc.Student).Where(doc => doc.StudentId == Id).ToList();
            //var documents = _context.Documents.Where(std => std.StudentId == 3).ToList()/*;*/
            var documentTypes = GetTypeNKDV();
            List<DocumentResponse> lstdocuments = new List<DocumentResponse>();
            documents.ForEach(p =>
            {
                DocumentResponse document = new DocumentResponse();
                document.Id = p.Id;
                document.Name = p.Name;
                document.Receiver = p.Receiver;
                document.ReceivedDate = p.ReceivedDate.ToString("yyyy/MM/dd");
                document.Submitter = p.Submitter;
                document.StudentName = p.Student.IdentificationCard.Name;
                document.Type = p.Type.ToUpper();
                document.Note = p.Note;
                document.Link = p.Link;
                document.StudentId = p.StudentId;
                lstdocuments.Add(document);
            });
            if (lstdocuments == null) throw new KeyNotFoundException("Document not found");
            IEnumerable<DocumentResponse> result = lstdocuments.Where(p => CheckTypeHS(p.Type, documentTypes));
            return result;
        }
        public IEnumerable<DocumentResponse> GetDocument3sByStudentId(int Id)
        {
            var documents = _context.Documents.Include(doc => doc.Student)
                .ThenInclude(stu => stu.IdentificationCard)
                .Include(doc => doc.Student).Where(doc => doc.StudentId == Id).ToList();
            //var documents = _context.Documents.Where(std => std.StudentId == 3).ToList()/*;*/
            var documentTypes = GetTypeSchool();
            List<DocumentResponse> lstdocuments = new List<DocumentResponse>();
            documents.ForEach(p =>
            {
                DocumentResponse document = new DocumentResponse();
                document.Id = p.Id;
                document.Name = p.Name;
                document.Receiver = p.Receiver;
                document.ReceivedDate = p.ReceivedDate.ToString("yyyy/MM/dd");
                document.Submitter = p.Submitter;
                document.StudentName = p.Student.IdentificationCard.Name;
                document.Type = p.Type.ToUpper();
                document.Note = p.Note;
                document.Link = p.Link;
                document.StudentId = p.StudentId;
                lstdocuments.Add(document);
            });
            if (lstdocuments == null) throw new KeyNotFoundException("Document not found");
            IEnumerable<DocumentResponse> result = lstdocuments.Where(p => CheckTypeHS(p.Type, documentTypes));
            return result;
        }
        public IEnumerable<DocumentTypeResponse> GetTypeHS()
        {
            List<DocumentTypeResponse> lsttypes = new List<DocumentTypeResponse>();
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Học bạ THPT" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Bằng tốt nghiệp THPT" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Bằng tốt nghiệp Trung cấp, Cao đẳng, Đại học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Bảng điểm Trung cấp, Cao đẳng, Đại học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Chứng chỉ tiếng Nhật" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Giấy khai sinh" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]CMND/CCCD" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Sổ hộ khẩu" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Bằng cấp, Giấy chứng nhận khác" });
            IEnumerable<DocumentTypeResponse> listTypeDocument = lsttypes as IEnumerable<DocumentTypeResponse>;
            return listTypeDocument;
        }
        public IEnumerable<DocumentTypeResponse> GetTypeNKDV()
        {
            List<DocumentTypeResponse> lsttypes = new List<DocumentTypeResponse>();
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Học bạ THPT" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Bằng tốt nghiệp THPT" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Bằng tốt nghiệp Trung cấp, Cao đẳng, Đại học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Bảng điểm Trung cấp, Cao đẳng, Đại học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Chứng chỉ tiếng Nhật" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Giấy khai sinh" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]CMND/CCCD" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Sổ hộ khẩu" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Bằng cấp, Giấy chứng nhận khác" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Xác nhận số dư, sổ tiết kiệm" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Bản giải thích về quá trình hình thành tài sản" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Giải trình khác" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Học bạ THPT" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Bằng tốt nghiệp THPT" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Bằng tốt nghiệp Trung cấp, Cao đẳng, Đại học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Bảng điểm Trung cấp, Cao đẳng, Đại học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Chứng chỉ tiếng Nhật" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Giấy khai sinh" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]CMND/CCCD" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Sổ hộ khẩu" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Bằng cấp, Giấy chứng nhận khác" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Xác nhận số dư, sổ tiết kiệm" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Bản giải thích về quá trình hình thành tài sản" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Giải trình khác" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Hình thành tiền" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Đơn xin nhập học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Đơn xin bảo lãnh" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]File scan hồ sơ đã hoàn thiện" });
            IEnumerable<DocumentTypeResponse> listTypeDocument = lsttypes as IEnumerable<DocumentTypeResponse>;
            return listTypeDocument;
        }
        public IEnumerable<DocumentTypeResponse> GetTypeSchool()
        {
            List<DocumentTypeResponse> lsttypes = new List<DocumentTypeResponse>();
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Hồ sơ trường Nhật gửi" });
            IEnumerable<DocumentTypeResponse> listTypeDocument = lsttypes as IEnumerable<DocumentTypeResponse>;
            return listTypeDocument;
        }
        public IEnumerable<CanDocumentResponse> GetCanDocumentByStudentId(int Id)
        {
            var lstdocuments = GetDocumentsByStudentId(Id).ToList();
            List<CanDocumentResponse> lsttypes = new List<CanDocumentResponse>();
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[HS]Học bạ THPT", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[HS]Bằng tốt nghiệp THPT", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[HS]Bằng tốt nghiệp Trung cấp, Cao đẳng, Đại học", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[HS]Bảng điểm Trung cấp, Cao đẳng, Đại học", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[HS]Chứng chỉ tiếng Nhật", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[HS]Giấy khai sinh", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[HS]CMND/CCCD", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[HS]Sổ hộ khẩu", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[HS]Bằng cấp, Giấy chứng nhận khác", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[VN]Học bạ THPT", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[VN]Bằng tốt nghiệp THPT", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[VN]Bằng tốt nghiệp Trung cấp, Cao đẳng, Đại học", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[VN]Bảng điểm Trung cấp, Cao đẳng, Đại học", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[VN]Chứng chỉ tiếng Nhật", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[VN]Giấy khai sinh", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[VN]CMND/CCCD", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[VN]Sổ hộ khẩu", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[VN]Bằng cấp, Giấy chứng nhận khác", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[VN]Xác nhận số dư, sổ tiết kiệm", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[VN]Bản giải thích về quá trình hình thành tài sản", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[VN]Giải trình khác", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Học bạ THPT", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Bằng tốt nghiệp THPT", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Bằng tốt nghiệp Trung cấp, Cao đẳng, Đại học", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Bảng điểm Trung cấp, Cao đẳng, Đại học", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Chứng chỉ tiếng Nhật", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Giấy khai sinh", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]CMND/CCCD", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Sổ hộ khẩu", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Bằng cấp, Giấy chứng nhận khác", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Xác nhận số dư, sổ tiết kiệm", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Bản giải thích về quá trình hình thành tài sản", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Giải trình khác", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Hình thành tiền", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Đơn xin nhập học", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Đơn xin bảo lãnh", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]File scan hồ sơ đã hoàn thiện", "Chưa bổ sung" } });
            lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { "[JP]Hồ sơ trường Nhật gửi", "Chưa bổ sung" } });
            lstdocuments.ForEach(item => {
                var check = lsttypes.Where(p => p.Type[0].ToUpper() == item.Type).FirstOrDefault();
                lsttypes.Remove(check);
                lsttypes.Add(new CanDocumentResponse() { Type = new List<string> { item.Type } });
            });
            var i = 1;
            lsttypes.ForEach(item => {
                item.Id = i; 
                i++;
            });
            return lsttypes;
        }
        public bool UpdateDocument(UpdateDocumentRequest data)
        {
            bool flag = false;
            TimeSpan ts = new TimeSpan(0, 0, 0);
            var document = _context.Documents.Find(data.Id);
            var b = DateTime.Parse(data.ReceivedDate);
            b = b.Date + ts;
            if (document != null)
            {
                document.ReceivedDate = b;
                document.Name = data.Name;
                document.Receiver = data.Receiver;
                document.Link = data.Link.ToString();
                document.Type = data.Type.ToLower();
                document.Note = data.Note;
                try
                {
                    _context.SaveChanges();
                    flag = true;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return flag;
        }
        public bool CreateDocument(DataDocumentRequest data)
        {
            bool flag = false;
            TimeSpan ts = new TimeSpan(0, 0, 0);
            try
            {
                var result = (from stu in _context.Students
                              join iden in _context.IdentificationCards on stu.IdentificationCardOfStudentId equals iden.Id into StudentInfo
                              from datas in StudentInfo.DefaultIfEmpty()
                              select new
                              {
                                  StudentName = datas.Name,
                                  Id = stu.Id
                              });
                Document document = new Document
                {
                    Name = data.Name,
                    ReceivedDate = data.ReceivedDate.Date + ts,
                    Receiver = data.Receiver,
                    //Link = data.Link.ToString(),
                    Type = data.Type.ToLower(),
                    Submitter = data.Submitter,
                    Note = data.Note,
                    StudentId = data.StudentId
                };
                _context.Documents.Add(document);
                _context.SaveChanges();
                flag = true;
            }
            catch (Exception ex)
            {
                throw;
            }
            return flag;
        }
    }
}
