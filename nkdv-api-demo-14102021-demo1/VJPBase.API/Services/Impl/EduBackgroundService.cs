﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;
using NKVD.Entities.Models;

namespace NKVD.API.Services.Impl
{
    public class EduBackgroundService : IEduBackgroundService
    {
        private NKDVDbContext _context;
        public EduBackgroundService(NKDVDbContext context)
        {
            _context = context;
        }

        public bool CreateBackground(int stuid,EduBackgroundRequest model)
        {
            try
            {
                var edu = new EduBackground()
                {
                    SchoolName = model.SchoolName,
                    Form = Convert.ToDateTime(model.Form),
                    To = Convert.ToDateTime(model.To),
                    Address = model.Address,
                    StudentId = stuid
                };
                _context.EduBackgrounds.Add(edu);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
                throw ex.InnerException;
            }
            return true;
        }

        public bool DeleteBackgroundbyId(int eduid)
        {
            try
            {
                var edu = _context.EduBackgrounds.Find(eduid);
                _context.EduBackgrounds.Remove(edu);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
                throw ex.InnerException;
            }
            return true;
        }

        public EduBackgroundRespone GetBackgroundbyId(int eduid)
        {
            var edu = _context.EduBackgrounds.Find(eduid);
            if(edu != null)
            {
                var eduresponse = new EduBackgroundRespone()
                {
                    Id = edu.Id,
                    SchoolName = edu.SchoolName,
                    Form = edu.Form.ToString("dd/MM/yyyy"),
                    To = edu.To.ToString("dd/MM/yyyy"),
                    Address = edu.Address
                };
                return eduresponse;
            }
            return null;
        }

        public IEnumerable<EduBackgroundRespone> GetListEduBackgroud(int studentId)
        {
            var listedu = _context.EduBackgrounds.Where(e => e.StudentId == studentId).Select(s => 
            new EduBackgroundRespone()
            {
                Id = s.Id,
                SchoolName = s.SchoolName,
                Form = s.Form.ToString("dd/MM/yyyy"),
                To = s.To.ToString("dd/MM/yyyy"),
                Address = s.Address,
            }).ToList();
            return listedu;
        }

        public bool UpdateBackgroundbyId(int id, EduBackgroundRequest model)
        {
            var edu = _context.EduBackgrounds.Find(id);
            if(edu != null)
            {
                edu.SchoolName = model.SchoolName;
                edu.Form = Convert.ToDateTime(model.Form);
                edu.To = Convert.ToDateTime(model.To);
                edu.Address = model.Address;
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex.InnerException; ;
                }
                return true;
            }
            return false;
        }
    }
}
