﻿
using Microsoft.EntityFrameworkCore;
using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.Entities.Models;
using NKVD.API.Models.Response.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;
using Document = NKVD.Entities.Models.Document;
using DocumentFormat.OpenXml.Packaging;
using System.IO;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Wordprocessing;

namespace NKVD.API.Services.Impl
{
    public class DocumentService : IDocumentService
    {
        private NKDVDbContext _context;
        public DocumentService(NKDVDbContext context)
        {
            _context = context;
        }
        public IEnumerable<DocumentResponse> GetAll()
        {
            var students = (from student in _context.Students
                            select new
                            {
                                StudentId = student.Id,
                                StudentName = student.IdentificationCard.Name
                            });
            List<DocumentResponse> results = (
                   from doc in _context.Documents
                   join stu in students on doc.StudentId equals stu.StudentId into DocumentofStudent
                   from data in DocumentofStudent.DefaultIfEmpty()
                   select new DocumentResponse
                   {
                       Id = doc.Id,
                       Name = doc.Name,
                       StudentName = data.StudentName,
                       ReceivedDate = doc.ReceivedDate.ToString("MM/dd/yyyy"),
                       Link = doc.Link,
                       Receiver = doc.Receiver,
                       Type = doc.Type.ToUpper(),
                       Note = doc.Note,
                       Submitter = doc.Submitter,
                       StudentId = doc.StudentId,
                   }
                   )?.ToList();
            var document = results.OrderByDescending(p=>p.ReceivedDate).AsEnumerable();
            //document.ForEach(x => { results.Remove(x); });
            if (document == null) throw new KeyNotFoundException("Document not found");
            //IEnumerable<DocumentResponse> listdocuments = document as IEnumerable<DocumentResponse>;
            return document;
        }

        public IEnumerable<DocumentResponse> GetDocumentsByStudentName(StudentNameSearchRequest value)
        {
            var documents = GetAll();
            List<DocumentResponse> results = documents.Where(p => string.IsNullOrWhiteSpace(value.StudentName) || p.StudentName.Contains(value.StudentName, StringComparison.OrdinalIgnoreCase)).ToList();
            if (results == null) throw new KeyNotFoundException("Document not found");
            IEnumerable<DocumentResponse> listdocuments = results as IEnumerable<DocumentResponse>;
            return listdocuments;
        }
        public bool UpdateDocument(DataDocumentRequest data)
        {
            bool flag = false;
            TimeSpan ts = new TimeSpan(0, 0, 0);
            var document = _context.Documents.Find(data.Id);
            //var b = DateTime.Parse(data.ReceivedDate);
            //b = b.Date + ts;
            if (document != null)
            {
                document.ReceivedDate = data.ReceivedDate;
                document.Name = data.Name;
                document.Receiver = data.Receiver;
                if (!String.IsNullOrEmpty(data.FileProfile)) document.Link = data.FileProfile;
                document.Type = data.Type.ToLower();
                document.Note = data.Note;
                document.StudentId = data.StudentId;
                try
                {
                    _context.SaveChanges();
                    flag = true;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return flag;
        }

        public IEnumerable<DocumentResponse> DeleteDocument(DocumentRequest data)
        {
            var document = _context.Documents.Where(p => p.Id == data.Id).FirstOrDefault();
            try
            {
                _context.Documents.Remove(document);
                _context.SaveChanges();
                return GetAll();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool CreateDocument(DataDocumentRequest data)
        {
            bool flag = false;
            try
            {
                //var result = (from stu in _context.Students
                //              join iden in _context.IdentificationCards on stu.IdentificationCardOfStudentId equals iden.Id into StudentInfo
                //              from datas in StudentInfo.DefaultIfEmpty()
                //              select new
                //              {
                //                  StudentName = datas.Name,
                //                  Id = stu.Id
                //              });
                //data.StudentId = (from r in result
                //                  where r.Id == data.StudentId
                //                  select r.Id).FirstOrDefault();

                Document document = new Document
                {
                    Name = data.Name,
                    ReceivedDate = data.ReceivedDate.Date,
                    Receiver = data.Receiver,
                    Link = data.FileProfile,
                    Type = data.Type.ToLower(),
                    Submitter = data.Submitter,
                    Note = data.Note,
                    StudentId = data.StudentId
                };
                _context.Documents.Add(document);
                _context.SaveChanges();
                flag = true;
            }
            catch (Exception ex)
            {
                throw;
            }
            return flag;
        }
        public DateTime ConvertDateTimeDeleteTime(string strDate, string format = "dd/MMM/yyyy")
        {
            DateTime temp = Convert.ToDateTime(strDate);
            var strDateFormat = temp.ToString(format);
            return Convert.ToDateTime(strDateFormat);
        }
        public IEnumerable<DocumentResponse> FilterDocument(FilterDocumentRequest obj)
        {
            //var grades = (from stug in _context.StudentGrades
            //              join gra in _context.Grades on stug.GradeId equals gra.Id into Grades
            //              from data in Grades.DefaultIfEmpty()
            //              select new
            //              {
            //                  StudentId = stug.StudentId,
            //                  GradeName = data.Name
            //              });
            var students = (from student in _context.Students
                            select new
                            {
                                StudentId = student.Id,
                                StudentName = student.IdentificationCard.Name,
                            });
            List<FilterDocumentResponse> filterDocuments = (
                   from data in _context.Documents
                   join stu in students on data.StudentId equals stu.StudentId into DocumentofStudent
                   from doc in DocumentofStudent.DefaultIfEmpty()
                   select new FilterDocumentResponse
                   {
                       Id = data.Id,
                       Name = data.Name,
                       StudentName = doc.StudentName,
                       ReceivedDate = data.ReceivedDate,
                       Link = data.Link,
                       Receiver = data.Receiver,
                       Type = data.Type.ToUpper(),
                       Note = data.Note,
                       Submitter = data.Submitter,
                       StudentId = data.StudentId,
                   }
                   )?.ToList();
            //List<FilterDocumentResponse> document = filterDocuments.FindAll(p => p.Id == null);
            //document.ForEach(x => { filterDocuments.Remove(x); });
            List<FilterDocumentResponse> results = new List<FilterDocumentResponse>();
            List<DocumentResponse> documents = new List<DocumentResponse>();
            TimeSpan ts = new TimeSpan(0, 0, 0);
            DateTime? _startDay = obj.StartDay != "Invalid Date" ? ConvertDateTimeDeleteTime(obj.StartDay) : null;
            DateTime? _endDay = obj.EndDay != "Invalid Date" ? ConvertDateTimeDeleteTime(obj.EndDay) : null;
           
            results = (from data in filterDocuments
                       where ((data.ReceivedDate >= _startDay) || _startDay == null) &&
                       (data.ReceivedDate <= _endDay || _endDay == null) &&
                       (data.Receiver == obj.Receiver || String.IsNullOrEmpty(obj.Receiver)) && (data.Type == obj.TypeDocument || String.IsNullOrEmpty(obj.TypeDocument))
                       select data).OrderByDescending(p => p.ReceivedDate).ToList();
            if (results != null)
            {
                results.ForEach(p =>
                {
                    DocumentResponse document = new DocumentResponse();
                    document.Id = p.Id;
                    document.Name = p.Name;
                    document.ReceivedDate = p.ReceivedDate?.ToString("MM/dd/yyyy");
                    document.Receiver = p.Receiver;
                    document.StudentId = p.StudentId;
                    document.StudentName = p.StudentName;
                    document.Submitter = p.Submitter;
                    document.Type = p.Type;
                    document.Note = p.Note;
                    document.Link = p.Link;
                    document.Grade = p.Grade;
                    documents.Add(document);

                });
            }
            if (results == null) throw new KeyNotFoundException("Document not found");
            IEnumerable<DocumentResponse> listdocuments = documents as IEnumerable<DocumentResponse>;
            return listdocuments;
        }

        public IEnumerable<DocumentReceiverResponse> GetReceiver()
        {
            List<DocumentReceiverResponse> receivers = new List<DocumentReceiverResponse>();
            //List<string> result = _context.Documents.Select(p => p.Receiver).Distinct().ToList();
            //result.ForEach(p =>
            //{
            //    DocumentReceiverResponse receiver = new DocumentReceiverResponse();
            //    receiver.Receiver = p;
            //    receivers.Add(receiver);
            //});
            receivers.Add(new DocumentReceiverResponse() { Receiver = "Nguyễn Thị Vân Khánh" });
            receivers.Add(new DocumentReceiverResponse() { Receiver = "Đặng Thị Cảnh" });
            receivers.Add(new DocumentReceiverResponse() { Receiver = "Nguyễn Thị Chung Thủy" });
            IEnumerable<DocumentReceiverResponse> listreceivers = receivers as IEnumerable<DocumentReceiverResponse>;
            return listreceivers;
        }
        public IEnumerable<DocumentStudentNameResponse> GetStudentName()
        {
            var students = (from student in _context.Students
                            select new DocumentStudentNameResponse
                            {
                                StudentId = student.Id,
                                StudentName = student.IdentificationCard.Name
                            }).OrderByDescending(p=>p.StudentName).ToList();
            //List <DocumentStudentNameResponse> result = _context.IdentificationCards.Select(p => new DocumentStudentNameResponse
            //{
            //    StudentId = p.Student.Id,
            //    StudentName = p.Name
            //}).Distinct().ToList();
            //IEnumerable<DocumentStudentNameResponse> liststu = result as IEnumerable<DocumentStudentNameResponse>;
            return students;
        }
        public IEnumerable<DocumentTypeResponse> GetType()
        {
            List<DocumentTypeResponse> lsttypes = new List<DocumentTypeResponse>();
            //List<string> result = _context.Documents.Select(p => p.Type).Distinct().ToList();
            //result.ForEach(p =>
            //{
            //    DocumentTypeResponse typedocument = new DocumentTypeResponse();
            //    typedocument.type = p.ToUpper();
            //    lsttypes.Add(typedocument);
            //});
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Học bạ THPT" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Bằng tốt nghiệp THPT" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Bằng tốt nghiệp Trung cấp, Cao đẳng, Đại học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Bảng điểm Trung cấp, Cao đẳng, Đại học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Chứng chỉ tiếng Nhật" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Giấy khai sinh" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]CMND/CCCD" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Sổ hộ khẩu" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[HS]Bằng cấp, Giấy chứng nhận khác" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Học bạ THPT" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Bằng tốt nghiệp THPT" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Bằng tốt nghiệp Trung cấp, Cao đẳng, Đại học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Bảng điểm Trung cấp, Cao đẳng, Đại học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Chứng chỉ tiếng Nhật" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Giấy khai sinh" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]CMND/CCCD" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Sổ hộ khẩu" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Bằng cấp, Giấy chứng nhận khác" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Xác nhận số dư, sổ tiết kiệm" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Bản giải thích về quá trình hình thành tài sản" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[VN]Giải trình khác" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Học bạ THPT" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Bằng tốt nghiệp THPT" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Bằng tốt nghiệp Trung cấp, Cao đẳng, Đại học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Bảng điểm Trung cấp, Cao đẳng, Đại học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Chứng chỉ tiếng Nhật" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Giấy khai sinh" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]CMND/CCCD" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Sổ hộ khẩu" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Bằng cấp, Giấy chứng nhận khác" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Xác nhận số dư, sổ tiết kiệm" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Bản giải thích về quá trình hình thành tài sản" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Giải trình khác" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Hình thành tiền" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Đơn xin nhập học" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Đơn xin bảo lãnh" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]File scan hồ sơ đã hoàn thiện" });
            lsttypes.Add(new DocumentTypeResponse() { type = "[JP]Hồ sơ trường Nhật gửi" });
            IEnumerable<DocumentTypeResponse> listTypeDocument = lsttypes as IEnumerable<DocumentTypeResponse>;
            return listTypeDocument;
        }
        private string FillDataWithJPCMND(string docText, int Id)
        {
            var std = _context.Students.Where(p => p.Id == Id).FirstOrDefault();
            var iden = _context.IdentificationCards.Where(p => p.Id == std.IdentificationCardOfStudentId).FirstOrDefault();
            Regex expression = new Regex(@"Username", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, RemoveUnicode(iden.Name).ToUpper());
            expression = new Regex(@"Y1", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Year.ToString());
            expression = new Regex(@"M1", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Month.ToString());
            expression = new Regex(@"D1", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Day.ToString());
            expression = new Regex(@"NoiCap", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, RemoveUnicode(iden.IssuedBy).ToUpper());
            expression = new Regex(@"KHKTT", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, RemoveUnicode(iden.PermanentAddress).ToUpper());
            expression = new Regex(@"KDK", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, RemoveUnicode(iden.CurrentAddress).ToUpper());
            expression = new Regex(@"Y2", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Date.Year.ToString());
            expression = new Regex(@"M2", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Date.Month.ToString());
            expression = new Regex(@"D2", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Date.Day.ToString());
            return docText;
        }
        private string FillDataWithVNXNCV(string docText, int Id)
        {
            var std = _context.Guardians.Where(p => p.GuardianOfStudentId == Id).FirstOrDefault();
            var iden = _context.IdentificationCards.Where(p => p.Id == std.IdentificationCardOfGuardianId).FirstOrDefault();
            var cpn = _context.Companys.Where(p => p.Id == std.CompanyOfGuardianId).FirstOrDefault();
            Regex expression = new Regex(@"K1N", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Name);
            expression = new Regex(@"Date", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.ToString("dd/MM/yyyy"));
            expression = new Regex(@"CMNDNumber", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.IdIdentificationCard);
            expression = new Regex(@"IssueD", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Date.ToString("dd/MM/yyyy"));
            expression = new Regex(@"IssueBy", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.IssuedBy);
            expression = new Regex(@"HKTTKey", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.PermanentAddress);
            expression = new Regex(@"NKD", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, cpn.BusinessProfession);
            expression = new Regex(@"YWokingTime", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, std.WorkingTime.Year.ToString());
            return docText;
        }
        private string FillDataWithJPHBTHPT(string docText, int Id)
        {
            var std = _context.Students.Where(p => p.Id == Id).FirstOrDefault();
            var iden = _context.IdentificationCards.Where(p => p.Id == std.IdentificationCardOfStudentId).FirstOrDefault();
            Regex expression = new Regex(@"HBPTK1", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, RemoveUnicode(iden.Name).ToUpper());
            expression = new Regex(@"HBPTK2", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Year.ToString());
            expression = new Regex(@"HBPTK3", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Month.ToString());
            expression = new Regex(@"HBPTK4", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Day.ToString());
            expression = new Regex(@"HBPTK5", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, RemoveUnicode(iden.BirthPlace).ToUpper());
            
            return docText;
        }
        private string FillDataWithJPBTNTHPT(string docText, int Id)
        {
            var std = _context.Students.Where(p => p.Id == Id).FirstOrDefault();
            var iden = _context.IdentificationCards.Where(p => p.Id == std.IdentificationCardOfStudentId).FirstOrDefault();
            Regex expression = new Regex(@"Username", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, RemoveUnicode(iden.Name).ToUpper());
            expression = new Regex(@"Y1", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Year.ToString());
            expression = new Regex(@"M1", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Month.ToString());
            expression = new Regex(@"D1", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Day.ToString());
            expression = new Regex(@"Birtplace", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, RemoveUnicode(iden.BirthPlace).ToUpper());

            return docText;
        }
        private string FillDataWithJPBTNDH(string docText, int Id)
        {
            var std = _context.Students.Where(p => p.Id == Id).FirstOrDefault();
            var iden = _context.IdentificationCards.Where(p => p.Id == std.IdentificationCardOfStudentId).FirstOrDefault();
            Regex expression = new Regex(@"Username", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, RemoveUnicode(iden.Name).ToUpper());
            expression = new Regex(@"Y1", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Year.ToString());
            expression = new Regex(@"M1", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Month.ToString());
            expression = new Regex(@"D1", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Day.ToString());
            return docText;
        }
        public string FillDataToDocument(WordprocessingDocument document,int Id, TypeExportRequets typeExport)
        {
            string docText = null;
            using (StreamReader sr = new StreamReader(document.MainDocumentPart.GetStream()))
            {
                docText = sr.ReadToEnd();
            }
            docText.ToString();

            if (typeExport.Type == 1) docText = FillDataWithVNXNCV(docText, Id);
            if (typeExport.Type == 2) docText = FillDataWithJPHBTHPT(docText, Id);
            if (typeExport.Type == 3) docText = FillDataWithJPBTNTHPT(docText, Id);
            if (typeExport.Type == 4) docText = FillDataWithJPCMND(docText, Id);
            if (typeExport.Type == 5) docText = FillDataWithJPBTNDH(docText, Id);
            if (typeExport.Type == 6) docText = FillDataWithJPCMND12(docText, Id);
            if (typeExport.Type == 7) docText = FillDataWithVNXNCVCompany(docText, Id);
            using (StreamWriter sw = new StreamWriter(document.MainDocumentPart.GetStream(FileMode.Create)))
            {
                sw.Write(docText);
                sw.Close();
            }
            return docText;
        }
        private string FillDataWithJPCMND12(string docText, int Id)
        {
            var std = _context.Students.Where(p => p.Id == Id).FirstOrDefault();
            var iden = _context.IdentificationCards.Where(p => p.Id == std.IdentificationCardOfStudentId).FirstOrDefault();
            Regex expression = new Regex(@"Username", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, RemoveUnicode(iden.Name).ToUpper());
            expression = new Regex(@"YYYY", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Year.ToString());
            expression = new Regex(@"MM", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Month.ToString());
            expression = new Regex(@"DD", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.Day.ToString());
            expression = new Regex(@"KHKTT", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, RemoveUnicode(iden.PermanentAddress).ToUpper());
            expression = new Regex(@"KNQ", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, RemoveUnicode(iden.HometownAddress).ToUpper());
            expression = new Regex(@"Y2", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Date.Year.ToString());
            expression = new Regex(@"M2", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Date.Month.ToString());
            expression = new Regex(@"D2", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Date.Day.ToString());
            return docText;
        }
        private string FillDataWithVNXNCVCompany(string docText, int Id)
        {
            var std = _context.Guardians.Where(p => p.GuardianOfStudentId == Id).FirstOrDefault();
            var iden = _context.IdentificationCards.Where(p => p.Id == std.IdentificationCardOfGuardianId).FirstOrDefault();
            var cpn = _context.Companys.Where(p => p.Id == std.CompanyOfGuardianId).FirstOrDefault();
            Regex expression = new Regex(@"GuadianName", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Name);
            expression = new Regex(@"GuadianBirthDate", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.Birthday.ToString("dd/MM/yyyy"));
            expression = new Regex(@"GuadianId", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.IdIdentificationCard);
            expression = new Regex(@"GdnDay", RegexOptions.IgnoreCase);
            var IdIssueDate = iden.Date.ToString("dd/MM/yyyy");
            docText = expression.Replace(docText, IdIssueDate);
            expression = new Regex(@"GdnIdIssueBy", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.IssuedBy);
            expression = new Regex(@"GuadianHKTT", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, iden.PermanentAddress);
            expression = new Regex(@"GuadianLevel", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, std.GuardianPosition);
            expression = new Regex(@"GuadianWorkTime", RegexOptions.IgnoreCase);
            var workTimeFormat = std.WorkingTime.Month.ToString() + @"/" + std.WorkingTime.Year.ToString();
            docText = expression.Replace(docText, workTimeFormat);

            expression = new Regex(@"COMPANYNAME", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, cpn.Name);

            expression = new Regex(@"CompanyAddress", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, cpn.Address);
            expression = new Regex(@"ComapanyTaxCode", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, cpn.TaxCode);
            expression = new Regex(@"Representative", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, cpn.Representative);
            expression = new Regex(@"PositionName ", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, std.RepresentativePosition);
            expression = new Regex(@"Position", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, std.RepresentativePosition);
            
            expression = new Regex(@"CompanyPhoneNumber", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, cpn.PhoneNumber);
            expression = new Regex(@"BusinessProfession", RegexOptions.IgnoreCase);
            docText = expression.Replace(docText, cpn.BusinessProfession);


            return docText;
        }
        public FileExport GetWordDocumentFromTemplateWithTempFile(int Id,TypeExportRequets typeExport)
        {
            string tempFileName = Path.GetTempFileName();
            //var templatePath = @"C:/Job/T.docx";
            var templatePath = "";

            //var pathTemp = @"/home/ec2-user/nkdv-api/VJPBase.API/Templates/";
            var pathTemp = Path.Combine(System.Environment.CurrentDirectory, @"Templates/");
            if (typeExport.Type == 1) templatePath = pathTemp + "VNXNCV.docx";
            if (typeExport.Type == 2) templatePath = pathTemp + "JPHBTHPT.docx";
            if (typeExport.Type == 3) templatePath = pathTemp + "JPBTNPHPT.docx";
            if (typeExport.Type == 4) templatePath = pathTemp + "JPCMND.docx";
            if (typeExport.Type == 5) templatePath = pathTemp + "JPBTNDH.docx";
            if (typeExport.Type == 6) templatePath = pathTemp + "JPCMND12.docx";
            if (typeExport.Type == 7) templatePath = pathTemp + "VNXNCVCompany.docx";
            using (var document = WordprocessingDocument.CreateFromTemplate(templatePath, true))
            {
                var docText = FillDataToDocument(document, Id, typeExport);

                //var filepath2 = @"C:/Job/T3.docx";
                //document.SaveAs(@filepath2);
                OpenXmlPackage savedDoc = document.SaveAs(tempFileName); // Save result document, not modifying the template
                savedDoc.Close();  // can't read if it's open
                document.Close();

                //memoryStream.Position = 0; // should I rewind it? 
            }
            var memoryStream = new MemoryStream(File.ReadAllBytes(tempFileName)); // this works but I want to avoid saving and reading file
            return new FileExport() { MemoryStream = memoryStream, FileName = tempFileName };
        }

        public string RemoveUnicode(string text)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
                                            "đ",
                                            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
                                            "í","ì","ỉ","ĩ","ị",
                                            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
                                            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
                                            "ý","ỳ","ỷ","ỹ","ỵ",};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
                                            "d",
                                            "e","e","e","e","e","e","e","e","e","e","e",
                                            "i","i","i","i","i",
                                            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
                                            "u","u","u","u","u","u","u","u","u","u","u",
                                            "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                text = text.Replace(arr1[i], arr2[i]);
                text = text.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return text;
        }

    }
}
