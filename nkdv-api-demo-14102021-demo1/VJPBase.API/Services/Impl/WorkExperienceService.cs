﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;

namespace NKVD.API.Services.Impl
{
    public class WorkExperienceService : IWorkExperiencesService
    {
        private NKDVDbContext _context;
        public WorkExperienceService(NKDVDbContext context)
        {
            _context = context;
        }
        public bool CreateWorkExperience(int stuid, WorkExperienceRequest model)
        {
            try
            {
                var exp = new WorkExperience()
                {
                    WorkName = model.WorkName,
                    Address = model.Address,
                    Position = model.Position,
                    Form = Convert.ToDateTime(model.Form),
                    To = Convert.ToDateTime(model.To),
                    StudentId = stuid
                };
                _context.WorkExperiences.Add(exp);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public bool DeleteWorkExperiencebyId(int expid)
        {
            try
            {
                var experience = _context.WorkExperiences.Find(expid);
                _context.WorkExperiences.Remove(experience);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
                throw ex.InnerException;
            }
            return true;
        }

        public IEnumerable<WorkExperienceResponse> GetListWorkExperience(int studentId)
        {
            var listexp = _context.WorkExperiences.Where(s => s.StudentId == studentId).Select(ex => 
            new WorkExperienceResponse() 
            { 
                Id = ex.Id,
                WorkName = ex.WorkName,
                Address = ex.Address,
                Position = ex.Position,
                Form = ex.Form.ToString("dd/MM/yyyy"),
                To = ex.To.ToString("dd/MM/yyyy")
            }).ToList();
            return listexp;
        }

        public WorkExperienceResponse GetWorkExperiencebyId(int expid)
        {
            var exp = _context.WorkExperiences.Where(s => s.Id == expid).Select(ex =>
            new WorkExperienceResponse()
            {
                Id = ex.Id,
                WorkName = ex.WorkName,
                Address = ex.Address,
                Position = ex.Position,
                Form = ex.Form.ToString("dd/MM/yyyy"),
                To = ex.To.ToString("dd/MM/yyyy")
            }).FirstOrDefault();
            return exp;
        }

        public bool UpdateWorkExperiencebyId(int id, WorkExperienceRequest model)
        {
            var exp = _context.WorkExperiences.Find(id);
            if (exp != null)
            {
                exp.WorkName = model.WorkName;
                exp.Address = model.Address;
                exp.Position = model.Position;
                exp.Form = Convert.ToDateTime(model.Form);
                exp.To = Convert.ToDateTime(model.To);
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex.InnerException; ;
                }
                return true;
            }
            return false;
        }
    }
}
