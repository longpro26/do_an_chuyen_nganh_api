﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;

namespace NKVD.API.Services.Impl
{
    public class GuardianService : IGuardianService
    {
        private NKDVDbContext _context;
        private readonly ILogger<GuardianService> _logger;
        public GuardianService(NKDVDbContext contextt)
        {
            _context = contextt;
        }

        public GuardianResponse GetByIdGuardian(int id)
        {
            var d = _context.Guardians.Where(p => p.GuardianOfStudentId == id).FirstOrDefault();
            if (d == null) return new GuardianResponse();
            
            var s = _context.IdentificationCards.Where(p => p.Id == d.IdentificationCardOfGuardianId).FirstOrDefault();
            if (s == null) return new GuardianResponse();
            
            var c = _context.Companys.Where(p => p.Id == d.CompanyOfGuardianId).FirstOrDefault();
            if (c == null) return new GuardianResponse();

            var result = new GuardianResponse()
            {
                //guardian
               
                WorkPlace = d.WorkPlace,
                PhoneNumber = d.PhoneNumber,
                RepresentativePosition = d.RepresentativePosition,
                WorkingTime = d.WorkingTime,
                GuardianPosition = d.GuardianPosition,
                TaxCode = d.TaxCode,
                //company
                NameCompany = c.Name,
                BusinessProfession = c.BusinessProfession,
                TaxCodeCompany = c.TaxCode,
                PhoneNumberCompany = c.PhoneNumber,
                Address = c.Address,
                Representative = c.Representative,
                //identificationcards
                Name = s.Name,
                Id = s.IdIdentificationCard,
                Date = s.Date,
                IssuedBy = s.IssuedBy,
                Sex = s.Sex,
                PermanentAddress = s.PermanentAddress,
                CurrentAddress = s.CurrentAddress,
                HometownAddress = s.HometownAddress,
                Birthday = s.Birthday,
                BirthPlace = s.BirthPlace,
            };

            return result;
        }
        public Boolean UpdateGuardianId(GuardianCreateRequest model)
        {

            var guardian = _context.Guardians.Where(p => p.GuardianOfStudentId == model.StudentId).FirstOrDefault();


            if (guardian != null)
            {
                var indentificationcard = _context.IdentificationCards.Find(guardian.IdentificationCardOfGuardianId);
                var company = _context.Companys.Find(guardian.CompanyOfGuardianId);

                indentificationcard.Name = model.IdenttificationCardName;
                indentificationcard.IdIdentificationCard = model.IdIdentificationCard.ToString();
                indentificationcard.Date = Convert.ToDateTime(model.Date);
                indentificationcard.IssuedBy = model.IssuedBy;
                indentificationcard.Sex = model.Sex;
                indentificationcard.PermanentAddress = model.PermanentAddress;
                indentificationcard.CurrentAddress = model.CurrentAddress;
                indentificationcard.HometownAddress = model.HometownAddress;
                indentificationcard.Birthday = Convert.ToDateTime(model.Birthday);
                indentificationcard.BirthPlace = model.BirthPlace;
                //company
                company.Name = model.CompanyName;
                company.BusinessProfession = model.BusinessProfession;
                company.TaxCode = model.TaxCodeCompany;
                company.Address = model.Address;
                company.PhoneNumber = model.PhoneNumberCompany;
                company.Representative = model.Representative;
                company.Representative = model.Representative;

                //guardian

                guardian.WorkPlace = model.WorkPlace;
                guardian.PhoneNumber = model.PhoneNumberGuardian;
                guardian.RepresentativePosition = model.RepresentativePosition;
                guardian.WorkingTime = Convert.ToDateTime(model.WorkingTime);
                guardian.TaxCode = model.TaxCodeGuardian;
                guardian.GuardianPosition = model.GuardianPosition;

                try
                {
                    _context.IdentificationCards.Update(indentificationcard);
                    _context.Companys.Update(company);
                    _context.Guardians.Update(guardian);
                    _context.SaveChanges();

                }
                catch (Exception ex)
                {
                    return false;
                }
                return true;

            }
            return false;
        }

        public bool CreateOrUpdateGuardianInfo(GuardianCreateRequest model)
        {
            try
            {
                var guardian = new Guardian()
                {
                    GuardianOfStudentId = model.StudentId,
                    WorkPlace = model.WorkPlace,
                    PhoneNumber = model.PhoneNumberGuardian,
                    RepresentativePosition = model.RepresentativePosition,
                    WorkingTime = Convert.ToDateTime(model.WorkingTime),
                    TaxCode = model.TaxCodeGuardian,
                    GuardianPosition = model.GuardianPosition,

                    IdentificationCard = new IdentificationCard()
                    {
                        IdIdentificationCard = model.IdIdentificationCard.ToString(),
                        Name = model.IdenttificationCardName,
                        Date = Convert.ToDateTime(model.Date),
                        IssuedBy = model.IssuedBy,
                        Sex = model.Sex,
                        PermanentAddress = model.PermanentAddress,
                        CurrentAddress = model.CurrentAddress,
                        HometownAddress = model.HometownAddress,
                        Birthday = Convert.ToDateTime(model.Birthday),
                        BirthPlace = model.BirthPlace

                    },
                    Company = new Company()
                    {
                        Name = model.CompanyName,
                        BusinessProfession = model.BusinessProfession,
                        TaxCode = model.TaxCodeCompany,
                        Address = model.Address,
                        PhoneNumber = model.PhoneNumberCompany,
                        Representative = model.Representative,
                    },
                };
                var check = _context.Guardians.Where(p => p.GuardianOfStudentId == model.StudentId).Count();

                if (check == 0)
                {
                    _context.Add(guardian);
                    //_context.IdentificationCards.Add(guardian.IdentificationCard);
                    //_context.Companys.Add(guardian.Company);
                    _context.SaveChanges();
                }
                else
                {
                    UpdateGuardianId(model);

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return true;
        }
    }
}
