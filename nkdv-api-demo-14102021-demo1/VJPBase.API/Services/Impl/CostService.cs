﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;

namespace NKVD.API.Services.Impl
{
    public class CostService : ICostService
    {
        private NKDVDbContext _context;

        public CostService(
            NKDVDbContext context
            )
        {
            _context = context;      
        }

        public List<Cost> GetAllCost()
        {
            return _context.Costs.ToList();
        }

        public CostResponse GetCostbyId(int costId)
        {
            var cost = GetAllCost();
            CostResponse response = cost.Where(c => c.TotalCostOfStudentId == costId).Select(a => new CostResponse() { 
            Id = a.Id,
            TotalPrice = a.TotalPrice,
            ContractSigningDate = a.ContractSigningDate,
            Note = a.Note,
            TotalCostOfStudentId = a.TotalCostOfStudentId
            }).FirstOrDefault();
            return response;
        }
        public bool CheckUpdate(int id)
        {
            var check = _context.Costs.Where(p=>p.TotalCostOfStudentId == id).Count();
            if (check == 0) return false;
            return true;

        }
        public bool InsertCost(CostRequest model)
        {
            try
            {
                var cost = new Cost()
                {
                    TotalPrice = model.TotalPrice,
                    ContractSigningDate = Convert.ToDateTime(model.ContractSigningDate),
                    Note = model.Note,                
                    TotalCostOfStudentId = model.TotalCostOfStudentId
                };
                _context.Costs.Add(cost);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;

        }

        public List<NKVD.API.Models.Response.Dto.TypeCostRespon> GetTypeCost()
        {
            List<NKVD.API.Models.Response.Dto.TypeCostRespon> lst = new List<NKVD.API.Models.Response.Dto.TypeCostRespon>();

            var types = _context.Types.ToList();
            types.ForEach(item => lst.Add(new NKVD.API.Models.Response.Dto.TypeCostRespon() { Id = item.Id, TypeName = item.Name}));
            return lst;
        }
        public bool UpdateCost(int id, CostRequest model)
        {
            var cost = _context.Costs.Where(p => p.TotalCostOfStudentId == id).FirstOrDefault();
            if (cost != null)
            {
                cost.TotalPrice = model.TotalPrice;
                cost.ContractSigningDate = Convert.ToDateTime(model.ContractSigningDate);
                cost.Note = model.Note;         
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex.InnerException; ;
                }
                return true;
            }
            return false;

        }
    }
}
