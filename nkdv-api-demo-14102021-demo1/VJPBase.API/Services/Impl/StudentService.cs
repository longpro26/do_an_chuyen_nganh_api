﻿using Microsoft.EntityFrameworkCore;
using BCryptNet = BCrypt.Net.BCrypt;
using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;
using VJPBase.API.Models.Requests;
using VJPBase.API.Models.Response;

namespace NKVD.API.Services.Impl
{
    public class StudentService : IStudentService
    {
        private readonly NKDVDbContext _context;

        public StudentService(
            NKDVDbContext context)
        {
            _context = context;
        }

        public List<StudentInfoResponse> GetAllStudentTotal()
        {
            var DetailedCosts = _context.DetailedCosts.ToList();
            var Costs = _context.Costs.ToList();
            var Students = _context.Students.ToList();
            var IdentificationCards = _context.IdentificationCards.ToList();
            var Student = (
                    from stu in Students
                    join info in IdentificationCards on stu.IdentificationCardOfStudentId equals info.Id into student
                    from stu_info in student.DefaultIfEmpty()
                    select new { stu, stu_info }
                    ).ToList();

            var studentinfo = (
                    from s in Student
                    join cost in Costs on s.stu.Id equals cost.TotalCostOfStudentId into stu_cost
                    from sc in stu_cost.DefaultIfEmpty()
                    join d in DetailedCosts on s.stu.Id equals d.CostOfStudentId into studentsss
                    from ss in studentsss.DefaultIfEmpty()
                    select new { s, ss, sc }
                    ).ToList();

            var lst = studentinfo.GroupBy(gr => gr.s.stu.Id).Select(p => new StudentInfoResponse
            {
                StudentId = p.Key,
                Name = p.Select(s => s.s.stu_info.Name).FirstOrDefault(),
                PhoneNumber = p.Select(s => s.s.stu.PhoneNumber).FirstOrDefault(),
                FacebookUrl = p.Select(s => s.s.stu.FacebookUrl).FirstOrDefault(),
                CurrentAddress = p.Select(s => s.s.stu_info.CurrentAddress).FirstOrDefault(),
                Piece = p.Select(s => (s.ss == null ? 0 : s.ss.Piece)).Sum(),
                TotalPrice = p.Select(s => s.sc == null ? 0 : s.sc.TotalPrice).FirstOrDefault(),
                BirthDate = p.Select(p => p.s.stu_info.Birthday).FirstOrDefault().ToString("dd/MM/yyyy"),
            }).ToList();
            return lst;
        }
        public List<StudentInfoResponse> GetTuitionOfStudent()
        {
            var DetailedCosts = _context.DetailedCosts.ToList();
            var Costs = _context.Costs.ToList();
            var Students = _context.Students.ToList();
            var IdentificationCards = _context.IdentificationCards.ToList();
            var Student = (
                    from stu in Students
                    join info in IdentificationCards on stu.IdentificationCardOfStudentId equals info.Id into student
                    from stu_info in student.DefaultIfEmpty()
                    select new { stu, stu_info }
                    ).ToList();

            var studentinfo = (
                    from s in Student
                    join cost in Costs on s.stu.Id equals cost.TotalCostOfStudentId
                    join d in DetailedCosts on s.stu.Id equals d.CostOfStudentId into studentsss
                    from ss in studentsss.DefaultIfEmpty()
                    select new { s, ss, cost }
                    ).ToList();

            var lst = studentinfo.GroupBy(gr => gr.s.stu.Id).Select(p => new StudentInfoResponse
            {
                StudentId = p.Key,
                Name = p.Select(s => s.s.stu_info.Name).FirstOrDefault(),
                PhoneNumber = p.Select(s => s.s.stu.PhoneNumber).FirstOrDefault(),
                FacebookUrl = p.Select(s => s.s.stu.FacebookUrl).FirstOrDefault(),
                CurrentAddress = p.Select(s => s.s.stu_info.CurrentAddress).FirstOrDefault(),
                Piece = p.Select(s => s.ss.Piece).Sum(),
                TotalPrice = p.Select(s => s.cost.TotalPrice).FirstOrDefault(),
                BirthDate = p.Select(p => p.s.stu_info.Birthday).FirstOrDefault().ToString("dd/MM/yyyy"),
            }).ToList();
            return lst;
        }

        public List<StudentInfoResponse> GetTuitionOfStudent1()
        {
            var lst = GetAllStudentTotal();
            lst = lst.Where(p => p.TotalPrice > p.Piece || p.TotalPrice == 0).ToList();

            return lst;
        }
        public List<StudentInfoResponse> GetTuitionOfStudent2()
        {
            var lst = GetAllStudentTotal();
            lst = lst.Where(p => p.TotalPrice <= p.Piece && p.TotalPrice != 0).ToList();

            return lst;
        }

        ////Show list student
        //public List<StudentResponse> GetAllStudent()
        //{
        //    var DetailedCosts = _context.DetailedCosts.ToList();
        //    var Costs = _context.Costs.ToList();
        //    var Students = _context.Students.ToList();
        //    var IdentificationCards = _context.IdentificationCards.ToList();
        //    var Student = (
        //            from sts in Students
        //            join ifc in IdentificationCards on sts.IdentificationCardOfStudentId equals ifc.Id into student
        //            from st in student.DefaultIfEmpty()
        //            select new { sts, st }
        //            ).ToList();

        //    var detailedcosts = (
        //            from dcs in DetailedCosts
        //            join cost in Costs on dcs.CostId equals cost.Id
        //            join student in Student on dcs.StudentId equals student.sts.Id into detailedcost
        //            from dc in detailedcost.DefaultIfEmpty()
        //            select new { dcs, dc }
        //            ).ToList();
        //    var lst = detailedcosts.Select(p => new StudentResponse
        //    {
        //        Id = p.dc.sts.Id,
        //        Name = p.dc.st.Name,
        //        PhoneNumber = p.dc.sts.PhoneNumber,
        //        FacebookUrl = p.dc.sts.FacebookUrl,
        //        CurrentAddress = p.dc.st.CurrentAddress,
        //        Price = p.dcs.Cost.Price
        //    }).ToList();
        //    return lst;
        //}

        //Show student who paid tuition
        public List<StudentResponse> GetCostStudent()
        {
            var DetailedCosts = _context.DetailedCosts.ToList();
            var Costs = _context.Costs.ToList();
            var Students = _context.Students.ToList();
            var IdentificationCards = _context.IdentificationCards.ToList();
            var Student = (
                    from sts in Students
                    join ifc in IdentificationCards on sts.IdentificationCardOfStudentId equals ifc.Id into student
                    from st in student.DefaultIfEmpty()
                    select new { sts, st }
                    ).ToList();

            var detailedcosts = (
                    from dcs in DetailedCosts
                    join cost in Costs on dcs.CostOfStudentId equals cost.Id
                    join student in Student on dcs.CostOfStudentId equals student.sts.Id into detailedcost
                    from dc in detailedcost.DefaultIfEmpty()
                    select new { dcs, dc }
                    ).ToList();
            var lst = detailedcosts.GroupBy(p => p.dcs.CostOfStudentId).Select(p => new StudentResponse
            {
                Id = p.Key,
                Name = p.Select(p => p.dc.st.Name).FirstOrDefault(),
                PhoneNumber = p.Select(p => p.dc.sts.PhoneNumber).FirstOrDefault(),
                FacebookUrl = p.Select(p => p.dc.sts.FacebookUrl).FirstOrDefault(),
                CurrentAddress = p.Select(p => p.dc.st.CurrentAddress).FirstOrDefault(),
                Price = p.Select(p => p.dcs.Piece).Sum(),
                BirthDate = p.Select(p => p.dc.st.Birthday).FirstOrDefault().ToString("dd/MM/yyyy"),
            }).ToList();
            return lst;
        }
        public List<StudentResponse> GetTuitionDebtStudent()
        {
            var DetailedCosts = _context.DetailedCosts.ToList();
            var Costs = _context.Costs.ToList();
            var Students = _context.Students.ToList();
            var IdentificationCards = _context.IdentificationCards.ToList();
            var Student = (
                    from sts in Students
                    join ifc in IdentificationCards on sts.IdentificationCardOfStudentId equals ifc.Id into student
                    from st in student.DefaultIfEmpty()
                    select new { sts, st }
                    ).ToList();

            var detailedcosts = (
                    from dcs in DetailedCosts
                    join cost in Costs on dcs.CostOfStudentId equals cost.Id
                    join student in Student on dcs.CostOfStudentId equals student.sts.Id into detailedcost
                    from dc in detailedcost.DefaultIfEmpty()
                    select new { dcs, dc }
                    ).ToList();
            var lst = detailedcosts.GroupBy(p => p.dcs.CostOfStudentId).Select(p => new StudentResponse
            {
                Id = p.Key,
                Name = p.Select(p => p.dc.st.Name).FirstOrDefault(),
                PhoneNumber = p.Select(p => p.dc.sts.PhoneNumber).FirstOrDefault(),
                FacebookUrl = p.Select(p => p.dc.sts.FacebookUrl).FirstOrDefault(),
                CurrentAddress = p.Select(p => p.dc.st.CurrentAddress).FirstOrDefault(),
                BirthDate = p.Select(p => p.dc.st.Birthday).FirstOrDefault().ToString("dd/MM/yyyy")
            }).ToList();
            return lst;
        }

        public List<StudentInfoResponse> SearchName(FindStuOfNameRequest name)
        {
            //var a = GetAllStudent().Where(t => String.Compare(name.Name, t.Name, true) == 0).ToList();
            var a = GetAllStudentTotal().Where(p => string.IsNullOrWhiteSpace(name.Name) ||
            p.Name.IndexOf(name.Name, StringComparison.OrdinalIgnoreCase) != -1 || 
            p.PhoneNumber.IndexOf(name.Name, StringComparison.OrdinalIgnoreCase) != -1 ).ToList();
            return a;
        }
        public List<StudentInfoResponse> SearchNameByTuition1(FindStuOfNameRequest name)
        {
            //var a = GetAllStudent().Where(t => String.Compare(name.Name, t.Name, true) == 0).ToList();
            var a = GetTuitionOfStudent1().Where(p => string.IsNullOrWhiteSpace(name.Name) || p.Name.IndexOf(name.Name, StringComparison.OrdinalIgnoreCase) != -1 || p.PhoneNumber.IndexOf(name.Name, StringComparison.OrdinalIgnoreCase) != -1).ToList();
            return a;
        }

        public List<StudentInfoResponse> SearchNameByTuition2(FindStuOfNameRequest name)
        {
            //var a = GetAllStudent().Where(t => String.Compare(name.Name, t.Name, true) == 0).ToList();
            var a = GetTuitionOfStudent2().Where(p => string.IsNullOrWhiteSpace(name.Name) || p.Name.IndexOf(name.Name, StringComparison.OrdinalIgnoreCase) != -1 || p.PhoneNumber.IndexOf(name.Name, StringComparison.OrdinalIgnoreCase) != -1).ToList();
            return a;
        }
        public bool RemoveStudent(RemoveStudentRequest remove)
        {
            try
            {
                var list = _context.Students.ToList();
                _context.Remove(list.Single(p => p.Id == remove.Id));
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public List<Student> GetAllStudent()
        {
            return _context.Students.ToList();
        }
        public List<IdentificationCard> GetAllIdentificationCard()
        {
            return _context.IdentificationCards.ToList();
        }
        public List<Visa> GetAllVisa()
        {
            return _context.Visas.ToList();
        }
        public List<StudentGrade> GetAllStudentGrade()
        {
            return _context.StudentGrades.ToList();
        }
        //lấy danh sách lớp học
        public List<Grade> GetAllGrade()
        {
            return _context.Grades.ToList();
        }
        public List<InfoInJapan> GetAllInfoInJapans()
        {
            return _context.InfoInJapans.ToList();
        }
        public StudentInfoDetailResponse GetbyID(int id)
        {
            // lấy danh sách sinh viên trong bảng student
            var students = GetAllStudent();
            // lấy danh sách thông tin thẻ căn cước trong bảng IdentificationCards
            var identificationCards = GetAllIdentificationCard();
            // lấy danh sách visa
            var visas = GetAllVisa();
            // lấy danh sách lớp học của sinh viên 
            var studentGrades = GetAllStudentGrade();
            ////lấy danh sách lớp học
            //var grades = GetAllGrade();
            //lấy danh sách thông tin tại nhật
            var infojapans = GetAllInfoInJapans();

            //var gradesOfStudent = studentGrades.Join(grades, g => g.GradeId, sg => sg.Id, (StudentGrades, Grade) =>
            //      {
            //          return new
            //          {
            //              StudentId = StudentGrades.StudentId,
            //              NameGrade = Grade.Name,
            //              GradeId = Grade.Id
            //          };
            //      }).GroupBy(st => st.StudentId).Select(re =>
            //      {
            //          return new
            //          {
            //              StudentId = re.Key,
            //              NameGrade = re.Select(p => p.NameGrade).ToList(),
            //              GradeId = re.Select(p => p.GradeId).ToList()
            //          };
            //      }).ToList();

            var stulst = _context.Students.Include("Visa").Include("InfoInJapan").ToList().Where(student => student.Id == id)
                .Select(student =>
                {
                    return new StudentInfoDetailResponse
                    {
                        Id = student.Id,
                        PhoneNumber = student.PhoneNumber,
                        IdentificationCardOfStudentId = student.IdentificationCardOfStudentId,
                        FacebookUrl = student.FacebookUrl,
                        Gmail=student.Gmail,
                        //IdentificationCard
                        IdIdentificationCard = student.IdentificationCard.IdIdentificationCard,
                        Name = student.IdentificationCard.Name,
                        Date = student.IdentificationCard.Date.ToString("dd/MM/yyyy"),
                        IssuedBy = student.IdentificationCard.IssuedBy,
                        Sex = student.IdentificationCard.Sex,
                        PermanentAddress = student.IdentificationCard.PermanentAddress,
                        CurrentAddress = student.IdentificationCard.CurrentAddress,
                        HometownAddress = student.IdentificationCard.HometownAddress,
                        Birthday = student.IdentificationCard.Birthday.ToString("dd/MM/yyyy"),
                        BirthPlace = student.IdentificationCard.BirthPlace,
                        //visa
                        DateRange = student.Visa != null ? student.Visa.DateRange.ToString("dd/MM/yyyy") : null,
                        ExpirationDate = student.Visa != null ? student.Visa.ExpirationDate.ToString("dd/MM/yyyy") : null,
                        IssuedByVisa = student.Visa != null ? student.Visa.IssuedByVisa : null,
                        Type = student.Visa != null ? student.Visa.Type : null,
                        //grades
                        //NameGrades = grastu.NameGrade,
                        //infoinjapan
                        Address = student.InfoInJapan != null ? student.InfoInJapan.Address : null,
                        AreaCode = student.InfoInJapan != null ? student.InfoInJapan.AreaCode : null,
                        AddressType = student.InfoInJapan != null ? student.InfoInJapan.AddressType : null,
                        Majors = student.InfoInJapan != null ? student.InfoInJapan.Majors : null,
                        Specialized = student.InfoInJapan != null ? student.InfoInJapan.Specialized : null,
                        SchoolName = student.InfoInJapan != null ? student.InfoInJapan.SchoolName : null
                    };
                }).FirstOrDefault();
            return stulst;
        }

        public int CreateStudent(StudentRequest model)
        {
            var student = new Student()
            {
                Gmail=model.Gmail,
                PhoneNumber = model.PhoneNumber,
                FacebookUrl = model.FacebookUrl,
                IdentificationCard = new IdentificationCard()
                {
                    IdIdentificationCard = model.IdIdentificationCard,
                    Name = model.Name,
                    Date = Convert.ToDateTime(model.Date),
                    IssuedBy = model.IssuedBy,
                    Sex = model.Sex,
                    PermanentAddress = model.PermanentAddress,
                    CurrentAddress = model.CurrentAddress,
                    HometownAddress = model.HometownAddress,
                    Birthday = Convert.ToDateTime(model.Birthday),
                    BirthPlace = model.BirthPlace,
                },
                Visa = new Visa()
                {
                    DateRange = Convert.ToDateTime(model.DateRange),
                    ExpirationDate = Convert.ToDateTime(model.ExpirationDate),
                    IssuedByVisa = model.IssuedByVisa,
                    Type = model.Type
                },
                InfoInJapan = new InfoInJapan()
                {
                    Address = model.Address,
                    AreaCode = model.AreaCode,
                    AddressType = model.AddressType,
                    Majors = model.Majors,
                    Specialized = model.Majors,
                    SchoolName = model.SchoolName
                }
            };
          
            _context.Students.Add(student);
          
            _context.SaveChanges();
            return student.Id;
        }

        public bool UpdateStudent(int stuid, StudentRequest model)
        {
            // lấy danh sách sinh viên trong bảng student
            var stud = GetAllStudent().Where(p => p.Id == stuid).FirstOrDefault();
            // lấy danh sách thông tin thẻ căn cước trong bảng IdentificationCards
            var IdentificationCard = GetAllIdentificationCard().Where(p => p.Id == stud.IdentificationCardOfStudentId).FirstOrDefault();
            // lấy danh sách visa
            var vi = GetAllVisa().Where(p => p.VisaOfStudentId == stuid).FirstOrDefault();
            //lấy danh sách thông tin tại nhật
            var inja = GetAllInfoInJapans().Where(p => p.InfoInJapanOfStudentId == stuid).FirstOrDefault();
            stud.PhoneNumber = model.PhoneNumber;
            stud.FacebookUrl = model.FacebookUrl;
            stud.Gmail = model.Gmail;

            IdentificationCard.IdIdentificationCard = model.IdIdentificationCard;
            IdentificationCard.Name = model.Name;
            IdentificationCard.Date = Convert.ToDateTime(model.Date);
            IdentificationCard.IssuedBy = model.IssuedBy;
            IdentificationCard.Sex = model.Sex;
            IdentificationCard.PermanentAddress = model.PermanentAddress;
            IdentificationCard.CurrentAddress = model.CurrentAddress;
            IdentificationCard.HometownAddress = model.HometownAddress;
            IdentificationCard.Birthday = Convert.ToDateTime(model.Birthday);
            IdentificationCard.BirthPlace = model.BirthPlace;

            vi.DateRange = Convert.ToDateTime(model.DateRange);
            vi.ExpirationDate = Convert.ToDateTime(model.ExpirationDate);
            vi.IssuedByVisa = model.IssuedByVisa;
            vi.Type = model.Type;

            inja.Address = model.Address;
            inja.AreaCode = model.AreaCode;
            inja.AddressType = model.AddressType;
            inja.Majors = model.Majors;
            inja.Specialized = model.Specialized;
            inja.SchoolName = model.SchoolName;
            var user1 = _context.Users1.ToList();
            var checkgmail = user1.Where(w => w.UserName == model.Gmail).ToList();   
            var user = user1.Where(w => w.StudentID == stuid).FirstOrDefault();
            if (user==null)
            {
                if (checkgmail.Count >= 1)
                {
                    return false;
                }
                var acc = new Entities.Models.User1()
                {
                    Name = model.Name,
                    UserName=model.Gmail,
                    StudentID = stuid,
                    PasswordHash = BCryptNet.HashPassword(model.PhoneNumber),
                    Role = Role.Student
                };
                _context.Users1.Add(acc);

            }
            else
            {
                if (user.Name != model.Name)
                {
                    user.Name = model.Name;
                }
                if (user.UserName != model.Gmail)
                {
                    if (checkgmail.Count >= 1)
                    {
                        return false;
                    }
                    user.UserName = model.Gmail;
                }
                _context.Users1.Update(user);
            }         
            _context.Students.Update(stud);
            _context.IdentificationCards.Update(IdentificationCard);
            _context.Visas.Update(vi);
            _context.InfoInJapans.Update(inja);
            _context.SaveChanges();

            return true;
        }
        public bool ChangePassStudent(ChangePassStudentRequest request)
        {
            var user = _context.Users1.SingleOrDefault(x => x.UserName == request.Username);

            if (user == null || !BCryptNet.Verify(request.Passwordcu, user.PasswordHash))
                return false;

            user.PasswordHash = BCryptNet.HashPassword(request.PasswordNew) ;
            _context.Users1.Update(user);
            _context.SaveChanges();
            return true;
        }
        public int CountStudent()
        {
            var total = _context.Students.Count();
            if (total == null)
            {
                return 0;
            }
            return total;
        }
    }
}
