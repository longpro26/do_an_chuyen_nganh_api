﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;

namespace NKVD.API.Services.Impl
{
    public class DetailedCostsService : IDetailedCostService
    {
        private NKDVDbContext _context;

        public DetailedCostsService(
            NKDVDbContext context)
        {
            _context = context;
        }

        public bool DeleteDetailedCost(int detailedcostdId)
        {
            try
            {
                var detailedcost = _context.DetailedCosts.Find(detailedcostdId);
                _context.DetailedCosts.Remove(detailedcost);
                var typeCost = _context.TypeCosts.Where(p => p.DetailCostId == detailedcostdId).ToList();
                _context.TypeCosts.RemoveRange(typeCost);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
                throw ex.InnerException;
            }
            return true;
        }
        private List<Models.Response.Dto.TypeNameDto> GetTypeCost(int idDetailedCost, List<Models.Response.Dto.TypeNameDto> lstTypeName)
        {
            var a = lstTypeName.Where(p => p.IdCostDetail == idDetailedCost).ToList();
            return a;
        }
        public IEnumerable<DetailedCostsResponse> GetAllDetailedCost(int studentId)
        {
            var listStudent = _context.Students.ToList();
            var listIdentificationCard = _context.IdentificationCards.ToList();
            var listDetailedCost = _context.DetailedCosts.ToList();
            var detailCostType = (from t in _context.Types
                                  join tc in _context.TypeCosts on t.Id equals tc.TypeId
                                  select (new Models.Response.Dto.TypeNameDto
                                  {
                                      IdCostDetail = tc.DetailCostId,
                                      TypeName = t.Name,
                                      TypeId = tc.TypeId
                                  })).ToList();
            var Response =
                (from stu in listStudent
                 join dec in listDetailedCost on stu.Id equals dec.CostOfStudentId
                 where dec.CostOfStudentId == studentId
                 select (new DetailedCostsResponse()
                 {
                     Id = dec.Id,
                     CostOfStudentId = dec.CostOfStudentId,
                     Submitter = dec.Submitter,
                     Collector = dec.Collector,
                     DateOfLing = dec.DateOfLing,
                     Type = GetTypeCost(dec.Id, detailCostType),
                     Piece = dec.Piece,
                     Note = dec.Note,
                     NameStudent = stu.IdentificationCard.Name
                 })).ToList();

            return Response;
        }

        public DetailedCostsResponse GetDetailedCostbyId(int detailedcostdId)
        {
            var listDetailedCost = _context.DetailedCosts.ToList();
            var listStudent = _context.Students.ToList();
            var listIdentificationCard = _context.IdentificationCards.ToList();

            var Response =
                (from stu in listStudent
                 join dec in listDetailedCost on stu.Id equals dec.CostOfStudentId
                 where dec.CostOfStudentId == detailedcostdId
                 select (new DetailedCostsResponse()
                 {
                     DateOfLing = dec.DateOfLing,
                     NameStudent = stu.IdentificationCard.Name,
                     Collector = dec.Collector,
                     //Type = dec.Type,
                     Piece = dec.Piece,
                     Note = dec.Note
                 })).FirstOrDefault();
            return Response;
        }

        public bool InsertDetailedCost(DetailedCostsRequest model)
        {
            try
            {
                var detailedcost = new DetailedCost()
                {
                    DateOfLing = Convert.ToDateTime(model.DateOfLing),
                    Submitter = model.Submitter,
                    Collector = model.Collector,
                    //Type = model.Type,
                    Piece = model.Piece,
                    Note = model.Note,
                    CostOfStudentId = model.CostOfStudentId
                };

                _context.DetailedCosts.Add(detailedcost);
                _context.SaveChanges();

                model.Type.ForEach(item =>
                {
                    _context.TypeCosts.Add(new TypeCost
                    {
                        TypeId = item,
                        DetailCostId = detailedcost.Id
                    });
                });
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;

        }

        public bool UpdateDetailedCost(int id, DetailedCostsRequest model)
        {
            var detailedcost = _context.DetailedCosts.Find(id);
            if (detailedcost != null)
            {
                detailedcost.DateOfLing = Convert.ToDateTime(model.DateOfLing);
                // thieu ten hoc vien
                detailedcost.Collector = model.Collector;
                //detailedcost.Type = model.Type;
                detailedcost.Piece = model.Piece;
                detailedcost.Note = model.Note;

                _context.SaveChanges();
                var delTypeCost = _context.TypeCosts.Where(p => p.DetailCostId == id).ToList();
                _context.TypeCosts.RemoveRange(delTypeCost);
                _context.SaveChanges();

                model.Type.ForEach(item =>
                {
                    var a = new TypeCost
                    {
                        TypeId = item,
                        DetailCostId = id
                    };
                    _context.TypeCosts.Add(a);
                });
                _context.SaveChanges();
            }
            return false;

        }
    }
}
