﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.API.Models.Response.Dto;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;

namespace NKVD.API.Services.Impl
{
    public class CostOverview : ICostOverview
    {
        private NKDVDbContext _context;


        public CostOverview(NKDVDbContext context)
        {
            _context = context;
        }
        private List<Models.Response.Dto.TypeNameDto> GetTypeCost(int idDetailedCost, List<Models.Response.Dto.TypeNameDto> lstTypeName)
        {
            var a = lstTypeName.Where(p => p.IdCostDetail == idDetailedCost).ToList();
            return a;
        }
        public List<CostOverviewResponse> Costovervieww()
        {
            var DetailedCosts = _context.DetailedCosts.ToList();
            var Costs = _context.Costs.ToList();
            var Students = _context.Students.ToList();
            var IdentificationCards = _context.IdentificationCards.ToList();
            var Student = (
                    from sts in Students
                    join ifc in IdentificationCards on sts.IdentificationCardOfStudentId equals ifc.Id into student
                    from st in student.DefaultIfEmpty()
                    select new { sts, st }
                    ).ToList();
            //var Grades = _context.Grades.ToList();
            //var StudentGrades = _context.StudentGrades.ToList();
            //var StudentGrade = (from stg in StudentGrades
            //                    join st in Student on stg.StudentId equals st.sts.Id
            //                    join g in Grades on stg.GradeId equals g.Id into studentgrade
            //                    from sg in studentgrade.DefaultIfEmpty()
            //                    select new { stg, st, sg }).ToList();

            var detailedcosts = (
                    from dcs in DetailedCosts
                    join student in Student on dcs.CostOfStudentId equals student.st.Id into detailedcost
                    select new { dcs }
                    ).ToList();
            var detailCostType = (from t in _context.Types
                                  join tc in _context.TypeCosts on t.Id equals tc.TypeId
                                  select (new Models.Response.Dto.TypeNameDto
                                  {
                                      IdCostDetail = tc.DetailCostId,
                                      TypeName = t.Name,
                                      TypeId = tc.TypeId
                                  })).ToList();
            var list = detailedcosts.Select(s => new CostOverviewResponse
            {
                Stt = s.dcs.Id,
                DateOfFiling = s.dcs.DateOfLing,
                Submitter = s.dcs.Submitter,
                NameStudent = s.dcs.Student.IdentificationCard.Name,
                Collector = s.dcs.Collector,
                Type = GetTypeCost(s.dcs.Id, detailCostType),
                Price = s.dcs.Piece,
                Note = s.dcs.Note,
                CostOfStudentId = s.dcs.CostOfStudentId
            }).ToList();
            return list;
        }
        public List<CostOverview2Response> GetAllCostovervieww()
        {
            var costoverview = Costovervieww();
            var list = costoverview.Select(s => new CostOverview2Response
            {
                Stt = s.Stt,
                DateOfFiling = s.DateOfFiling.ToString("MM/dd/yyyy"),
                Submitter = s.Submitter,
                NameStudent = s.NameStudent,
                Collector = s.Collector,
                Type = s.Type,
                Price = s.Price,
                Note = s.Note,
                CostOfStudentId = s.CostOfStudentId

            }).ToList();
            return list;
        }
        public List<CostOverview2Response> SearchName(CostOverviewRequest mode)
        {
            dynamic key = mode.NameStudent;
            int number;
            var isNumeric = int.TryParse(mode.NameStudent, out _);
            if (!isNumeric)
            {
                number =0;
            }
            else
            {
                number = Int32.Parse(mode.NameStudent);
            }
            var lst = Costovervieww();
            var list = lst.Where(p => string.IsNullOrWhiteSpace(mode.NameStudent) || p.NameStudent.Contains(key, StringComparison.OrdinalIgnoreCase)
            ||p.Stt==number
            ||string.IsNullOrWhiteSpace(mode.NameStudent) || p.Collector.Contains(key, StringComparison.OrdinalIgnoreCase)
            || p.Price==number).ToList();
            var list1 = list.Select(s => new CostOverview2Response
            {
                Stt = s.Stt,
                DateOfFiling = s.DateOfFiling.ToString("MM/dd/yyyy"),
                Submitter = s.Submitter,
                NameStudent = s.NameStudent,
                Collector = s.Collector,
                Type = s.Type,
                Price = s.Price,
                Note = s.Note,
                CostOfStudentId = s.CostOfStudentId

            }).ToList();
            return list1;

        }
        public List<Grades> Grades()
        {
            var grades = _context.Grades.Select(s => new Grades
            {
                id = s.Id,
                Name = s.Name,
                OpenDate = s.OpenDate

            }).ToList();
            return grades;
        }
        public DateTime ConvertDateTimeDeleteTime(string strDate, string format = "dd/MMM/yyyy")
        {
            DateTime temp = Convert.ToDateTime(strDate);
            var strDateFormat = temp.ToString(format);
            return Convert.ToDateTime(strDateFormat);
        }
        public List<CostOverview2Response> FilterOverview(FilterOverviewRequest mode)
        {

            //var DetailedCosts = _context.DetailedCosts.ToList();
            //var Costs = _context.Costs.ToList();
            //var Students = _context.Students.ToList();
            //var IdentificationCards = _context.IdentificationCards.ToList();
            //var Student = (
            //        from sts in Students
            //        join ifc in IdentificationCards on sts.IdentificationCardOfStudentId equals ifc.Id into student
            //        from st in student.DefaultIfEmpty()
            //        select new { sts, st }
            //        ).ToList();
            //var Grades = _context.Grades.ToList();
            //var StudentGrades = _context.StudentGrades.ToList();
            //var StudentGrade = (from stg in StudentGrades
            //                    join st in Student on stg.StudentId equals st.sts.Id
            //                    join g in Grades on stg.GradeId equals g.Id into studentgrade
            //                    from sg in studentgrade.DefaultIfEmpty()
            //                    select new { stg, st, sg }).ToList();

            //var detailedcosts = (
            //    from dcs in DetailedCosts
            //    join student in StudentGrade on dcs.CostOfStudentId equals student.st.sts.Id into detailedcost
            //    from dc in detailedcost.DefaultIfEmpty()
            //    select new { dcs, dc }
            //    ).ToList();
            //TimeSpan ts = new TimeSpan(0, 0, 0);
            //mode.EndDate = mode.EndDate?.Date + ts;
            //mode.StarDate = mode.StarDate?.Date + ts;
            //var searchname = detailedcosts.Where(w =>
            //  (w.dcs.DateOfLing >= mode.StarDate && w.dcs.DateOfLing <= mode.EndDate) &&
            // w.dc.sg.Name == mode.Grade &&
            // w.dcs.Type == mode.Type).ToList();
            //if (mode.StarDate != null && mode.Grade == null && mode.Type == null)
            //{
            //    searchname = detailedcosts.Where(w => (w.dcs.DateOfLing >= mode.StarDate && w.dcs.DateOfLing <= mode.EndDate)).ToList();
            //}
            //if (mode.StarDate == null && mode.Grade != null & mode.Type != null)
            //{
            //    searchname = detailedcosts.Where(w => w.dc.sg.Name == mode.Grade && w.dcs.Type == mode.Type).ToList();
            //}
            //if (mode.StarDate == null && mode.Grade != null & mode.Type == null)
            //{
            //    searchname = detailedcosts.Where(w => w.dc.sg.Name == mode.Grade).ToList();
            //}
            //if (mode.StarDate == null && mode.Grade == null & mode.Type != null)
            //{
            //    searchname = detailedcosts.Where(w => w.dcs.Type == mode.Type).ToList();
            //}
            //if (mode.StarDate != null && mode.Grade != null & mode.Type == null)
            //{
            //    searchname = detailedcosts.Where(w => (w.dcs.DateOfLing >= mode.StarDate && w.dcs.DateOfLing <= mode.EndDate) && w.dc.sg.Name == mode.Grade).ToList();
            //}
            //if (mode.StarDate != null && mode.Grade == null & mode.Type != null)
            //{
            //    searchname = detailedcosts.Where(w => (w.dcs.DateOfLing >= mode.StarDate && w.dcs.DateOfLing <= mode.EndDate) && w.dcs.Type == mode.Type).ToList();
            //}
            var lst = Costovervieww();
            DateTime? _startDay = mode.StarDate != "Invalid Date" ? ConvertDateTimeDeleteTime(mode.StarDate) : null;
            DateTime? _endDay = mode.EndDate != "Invalid Date" ? ConvertDateTimeDeleteTime(mode.EndDate) : null;
            var searchname = lst.Where(p => (ConvertDateTimeDeleteTime(p.DateOfFiling.ToString()) >= _startDay || _startDay == null) &&
            (ConvertDateTimeDeleteTime(p.DateOfFiling.ToString()) <= _endDay || _endDay == null) &&
            (p.Type.Where(item => item.TypeName == mode.Type).Count() > 0 || string.IsNullOrEmpty(mode.Type)) &&
            (p.Grade == mode.Grade || string.IsNullOrEmpty(mode.Grade))).ToList();
            var list = searchname.Select(s => new CostOverview2Response
            {
                Stt = s.Stt,
                DateOfFiling = s.DateOfFiling.ToString("MM/dd/yyyy"),
                Submitter = s.Submitter,
                NameStudent = s.NameStudent,
                Collector = s.Collector,
                Type = s.Type,
                Price = s.Price,
                Note = s.Note,
                CostOfStudentId = s.CostOfStudentId

            }).ToList();
            return list;
        }
        public bool RemoveCostOverview(RemoverCostOverview mode)
        {
            try
            {
                var list = _context.DetailedCosts.ToList();
                _context.Remove(list.Single(f => f.Id == mode.Id));
                var typeCost = _context.TypeCosts.Where(p => p.DetailCostId == mode.Id).ToList();
                _context.TypeCosts.RemoveRange(typeCost);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }
        public List<CostOverview2Response> UpdateCostOverview(UpdateCostOverviewRequest mode)
        {
            try
            {
                DateTime Date = DateTime.Parse(mode.DateOfFiling);
                var detailedCosts = _context.DetailedCosts.First(f => f.Id == mode.Id);
                detailedCosts.DateOfLing = Date;
                //  detailedCosts.Submitter = mode.Submitter;
                detailedCosts.Collector = mode.Collector;
                //detailedCosts.Type = mode.Type;
                detailedCosts.Piece = mode.Price;
                detailedCosts.Note = mode.Note;
                _context.SaveChanges();
                var delTypeCost = _context.TypeCosts.Where(p => p.DetailCostId == mode.Id).ToList();
                _context.TypeCosts.RemoveRange(delTypeCost);
                _context.SaveChanges();

                mode.Type.ForEach(item =>
                {
                    var a = new TypeCost
                    {
                        TypeId = item,
                        DetailCostId = mode.Id
                    };
                    _context.TypeCosts.Add(a);
                });
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return GetAllCostovervieww();
            }

            return GetAllCostovervieww();
        }
    }
}
