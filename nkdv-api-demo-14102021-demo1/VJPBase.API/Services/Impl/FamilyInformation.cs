﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.API.Models.Response.Dto;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;


namespace NKVD.API.Services.Impl
{
    public class FamilyInformation : IFamilyInformation
    {
        private NKDVDbContext _context;

        public FamilyInformation(NKDVDbContext context)
        {
            _context = context;
        }
        public List<FamilyInformationResponse> Familyinformationn(int StudentId)
        {
            var FamilyMembers = _context.FamilyMembers.ToList();
            var list = FamilyMembers.Where(p => p.StudentId == StudentId).Select(s => new FamilyInformationResponse
            {
                Stt = s.Id,
                Relationship = s.Relationship,
                Name = s.Name,
                BirthDate = s.BirthDate.ToString("dd/MM/yyyy"),
                PhoneNumber = s.PhoneNumber,
                Address = s.Address,
                Job = s.Job,
            }).ToList();
            return list;
        }


        public bool DeleteFamilyinformationn(int Id)
        {
            try
            {
                var family = _context.FamilyMembers.Find(Id);
                _context.FamilyMembers.Remove(family);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
                throw ex.InnerException;
            }
            return true;
        }

        public bool UpdateFamilyinformationn(UpdateFamilyInformationRequest mode)
        {
            try
            {
                var FamilyMembers = _context.FamilyMembers.First(f => f.Id == mode.Stt);
                FamilyMembers.Relationship = mode.Relationship;
                FamilyMembers.Name = mode.Name;
                FamilyMembers.BirthDate = Convert.ToDateTime(mode.BirthDate);
                FamilyMembers.PhoneNumber = mode.PhoneNumber;
                FamilyMembers.Address = mode.Address;
                FamilyMembers.Job = mode.Job;
                _context.SaveChanges();

            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        public bool CreateFamilyinformationn(int stuid,AddFamilyInformationRequest mode)
        {
            try
            {
                var family = new Entities.Models.FamilyMember()
                {
                    Relationship = mode.Relationship,
                    Name = mode.Name,
                    BirthDate = Convert.ToDateTime(mode.BirthDate),
                    PhoneNumber = mode.PhoneNumber,
                    Address = mode.Address,
                    Job = mode.Job,
                    StudentId = stuid,
                };
                _context.FamilyMembers.Add(family);
                _context.SaveChanges();

            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

    }
}
