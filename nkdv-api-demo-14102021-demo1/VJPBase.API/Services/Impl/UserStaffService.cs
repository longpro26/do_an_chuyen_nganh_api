﻿
using BCryptNet = BCrypt.Net.BCrypt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;
using NKVD.Entities.Models;
using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;

namespace NKVD.API.Services.Impl
{
    public class UserStaffService:IUserStaffService
    {
        private NKDVDbContext _context;


        public UserStaffService(NKDVDbContext context)
        {
            _context = context;
        }
        public bool ReregisterUserStaff(RegisterUserStaffRequest request)
        {
            var checkemail = _context.Users1.FirstOrDefault(f => f.UserName == request.Email);
            if (checkemail != null)
            {
                return false;
            }
            if (request.Role==1)
            {
                var userstaff = new Entities.Models.User1
                {
                    UserName = request.Email,
                    Name = request.FullName,
                    Role = Role.User,
                    PasswordHash = BCryptNet.HashPassword(request.Password),
                    UserStaff=new Entities.Models.UserStaff
                    {
                        FullName=request.FullName,
                        Dateofbirth=request.Dateofbirth,
                        Address=request.Address,
                        PhoneNumber=request.PhoneNumber,
                        Email=request.Email
                    }
                };
                _context.Users1.Add(userstaff);
                _context.SaveChanges();
                return true;
            }
            if (request.Role == 0)
            {
                var userstaff = new Entities.Models.User1
                {
                    UserName = request.Email,
                    Name = request.FullName,
                    Role = Role.Admin,
                    PasswordHash = BCryptNet.HashPassword(request.Password),
                    UserAdmin = new Entities.Models.UserAdmin
                    {
                        FullName = request.FullName,
                        Dateofbirth = request.Dateofbirth,
                        Address = request.Address,
                        PhoneNumber = request.PhoneNumber,
                        Email = request.Email
                    }
                };
                _context.Users1.Add(userstaff);
                _context.SaveChanges();
                return true;
            }
            if (request.Role == 2)
            {
                var userstaff = new Entities.Models.User1
                {
                    UserName = request.Email,
                    Name = request.FullName,
                    Role = Role.Teacher,
                    PasswordHash = BCryptNet.HashPassword(request.Password),
                   Teacher = new Entities.Models.Teacher 
                   { 
                        LastName= request.FullName,
                        PhoneNumber = request.PhoneNumber,
                        Email = request.Email
                    }
                };
                _context.Users1.Add(userstaff);
                _context.SaveChanges();
                return true;
            }



            return false;
        }
        public List<UserManagementResponse> ListUserAdmin()
        {
            var list = _context.Users1.Where(w => w.Role == Role.Admin).Select(s =>
                     new UserManagementResponse
                     {
                         ID=s.Id,
                         FullName=s.Name,
                         Email=s.UserName,
                         PhoneNumber=s.UserAdmin.PhoneNumber,
                         Role="Admin"
                          

                     }).OrderByDescending(o=>o.ID).ToList();
            return list;
        }
        public List<UserManagementResponse> ListUserStaff()
        {
            var list = _context.Users1.Where(w => w.Role == Role.User).Select(s =>
                     new UserManagementResponse
                     {
                         ID = s.Id,
                         FullName = s.Name,
                         Email = s.UserName,
                         PhoneNumber = s.UserStaff.PhoneNumber,
                         Role = "User Staff"


                     }).OrderByDescending(o => o.ID).ToList();
            return list;
        }

        public List<UserManagementResponse> ListUserTeacher()
        {
            var list = _context.Users1.Where(w => w.Role == Role.Teacher).Select(s =>
                     new UserManagementResponse
                     {
                         ID = s.Id,
                         FullName = s.Name,
                         Email = s.UserName,
                         PhoneNumber = s.Teacher.PhoneNumber,
                         Role = "Teacher"


                     }).OrderByDescending(o => o.ID).ToList();
            return list;
        }

        public int CoutUserStaff()
        {
            var count = _context.UserStaffs.Count();
            if (count == null)
            {
                return 0;
            }
            return count;
        }
        public int CoutTeacher()
        {
            var count = _context.Teachers.Count();
            if (count == null)
            {
                return 0;
            }
            return count;
        }

    }
}
