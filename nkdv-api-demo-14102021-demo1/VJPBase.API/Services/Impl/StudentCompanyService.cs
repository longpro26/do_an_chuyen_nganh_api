﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;

namespace NKVD.API.Services.Impl
{
    public class StudentCompanyService : IStudentCompanyService
    {
        private NKDVDbContext _context;
        public StudentCompanyService(NKDVDbContext context)
        {
            _context = context;
        }
        public bool CreateStudentCompany(int stuid,StudentCompanyRequest model)
        {
            try
            {
                var com = new StudentCompany()
                {
                    StudentId = stuid,
                    Company = new Company()
                    {
                        Name = model.Name,
                        BusinessProfession = model.BusinessProfession,
                        TaxCode = model.TaxCode,
                        PhoneNumber = model.PhoneNumber,
                        Address = model.Address,
                        Representative = model.Representative
                    }                   
                };
                _context.StudentCompanys.Add(com);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public bool DeleteStudentCompanybyId(int comid)
        {
            try
            {
                var com = _context.Companys.Find(comid);
                _context.Companys.Remove(com);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
                throw ex.InnerException;
            }
            return true;
        }

        public StudentCompanyResponse GetStudentCompanybyId(int comid)
        {
            var co = _context.Companys.Find(comid);
            var companyresponse = new StudentCompanyResponse()
            {
                Name = co.Name,
                BusinessProfession = co.BusinessProfession,
                TaxCode = co.TaxCode,
                PhoneNumber = co.PhoneNumber,
                Address = co.Address,
                Representative = co.Representative,
            };
            return companyresponse;
        }

        public IEnumerable<StudentCompanyResponse> GetListStudentCompany(int studentId)
        {
            var listcompany = _context.Companys.ToList();
            var liststucomany = _context.StudentCompanys.ToList();
            var stucom = (from co in listcompany
                          join stc in  liststucomany on co.Id equals stc.CompanyId
                          where stc.StudentId == studentId
                          select new StudentCompanyResponse()
                          {
                              Id = co.Id,
                              Name = co.Name,
                              BusinessProfession = co.BusinessProfession,
                              TaxCode = co.TaxCode,
                              PhoneNumber = co.PhoneNumber,
                              Address = co.Address,
                              Representative = co.Representative,
                          }).ToList();
            return stucom;
        }

        public bool UpdateStudentCompanybyId(int id, StudentCompanyRequest model)
        {
            var company = _context.Companys.Find(id);
            if (company != null)
            {
                company.Name = model.Name;
                company.PhoneNumber = model.PhoneNumber;
                company.BusinessProfession = model.BusinessProfession;
                company.Address = model.Address;
                company.Representative = model.Representative;
                company.TaxCode = model.TaxCode;
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex.InnerException; ;
                }
                return true;
            }
            return false;
        }
    }
}
