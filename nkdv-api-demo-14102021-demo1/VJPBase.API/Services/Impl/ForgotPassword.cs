﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response.Dto;
using NKVD.Entities.Models;
using System.Linq;
using BCryptNet = BCrypt.Net.BCrypt;
using System.Threading.Tasks;
using VJPBase.API.Authorization;
using VJPBase.API.Helpers;


namespace NKVD.API.Services.Impl
{
    public class ForgotPassword: IForgotPassword
    {
        private ISendMailService _sendmail;
        private IJwtUtils _jwtUtils;
        private NKDVDbContext _context;


        public ForgotPassword(NKDVDbContext context, IJwtUtils jwtUtils, ISendMailService sendmail)
        {
            _context = context;
            _jwtUtils = jwtUtils;
            _sendmail = sendmail;
        }
        public string SendCode(ForgotPasswordEmailRequest request)
        {
            var user = _context.Users1.SingleOrDefault(x => x.UserName == request.email);
            if (user != null)
            {
                var jwtToken = _jwtUtils.GenerateJwtToken(user);
                user.Token = jwtToken;
                _context.Users1.Update(user);
                _context.SaveChanges();
                return jwtToken;
            }
          
            return "error";

        }
        public bool XacnhanUser(string email,string token)
        {
            var user = _context.Users1.SingleOrDefault(x => x.UserName == email&&x.Token==token) ;
            if (user != null)
            {
                return true;
            }
            return false;
        }
        public bool ResetPassword(ChangePasswordRequest request)
        {
            var resetpass = _context.Users1.SingleOrDefault(s => s.UserName == request.Username && s.Token == request.Token);
            if (resetpass != null)
            {
                var jwtToken = _jwtUtils.GenerateJwtToken(resetpass);
                resetpass.PasswordHash = BCryptNet.HashPassword(request.Password);
                resetpass.Token = jwtToken;
                _context.Users1.Update(resetpass);
                _context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
