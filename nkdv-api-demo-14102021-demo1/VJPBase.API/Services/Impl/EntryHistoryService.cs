﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Helpers;

namespace NKVD.API.Services.Impl
{
    public class EntryHistoryService : IEntryHistoryService
    {
        private NKDVDbContext _context;
        public EntryHistoryService(NKDVDbContext context)
        {
            _context = context;
        }

        public IEnumerable<EntryHistoryResponse> GetListEntryHistory(int studentId)
        {
            var listEntry = _context.PassEntryHistorys.Where(s => s.StudentId == studentId).Select(en => 
            new EntryHistoryResponse() 
            {
                Id = en.Id,
                EntryDate = en.EntryDate.ToString("dd/MM/yyyy"),
                OutputDate = en.OutputDate.ToString("dd/MM/yyyy"),
                PassportValidity = en.PassportValidity,
                Purpose = en.Purpose
            }).ToList();
            return listEntry;
        }

        public bool DeleteEntrybyId(int entryid)
        {
            try
            {
                var entry = _context.PassEntryHistorys.Find(entryid);
                _context.PassEntryHistorys.Remove(entry);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
                throw ex.InnerException;
            }
            return true;
        }

        public bool CreateEntry(int idst,EntryHistoryRequest passEntry)
        {
            try
            {
                var entry = new PassEntryHistory()
                {
                    EntryDate = Convert.ToDateTime(passEntry.EntryDate),
                    OutputDate = Convert.ToDateTime(passEntry.OutputDate),
                    PassportValidity =passEntry.PassportValidity,
                    Purpose = passEntry.Purpose,
                    StudentId = idst
                };
                _context.PassEntryHistorys.Add(entry);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public Boolean UpdateEntrybyId(int id,EntryHistoryRequest model)
        {
            var entry = _context.PassEntryHistorys.Find(id);
            if(entry != null)
            {
                entry.EntryDate = Convert.ToDateTime(model.EntryDate);
                entry.OutputDate = Convert.ToDateTime(model.OutputDate);
                entry.PassportValidity = model.PassportValidity;
                entry.Purpose = model.Purpose;
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                   throw ex.InnerException; ;
                }
                return true;
            }
            return false;
        }

        public EntryHistoryResponse GetEntrybyId(int entryid)
        {
            var entry = _context.PassEntryHistorys.Find(entryid);
            if(entry != null)
            {
                var entryresponse = new EntryHistoryResponse()
                {
                    EntryDate = entry.EntryDate.ToString("dd/MM/yyyy"),
                    OutputDate = entry.OutputDate.ToString("dd/MM/yyyy"),
                    PassportValidity = entry.PassportValidity,
                    Purpose = entry.Purpose
                };
                return entryresponse;
            }
            return null;
        }
    }
}
