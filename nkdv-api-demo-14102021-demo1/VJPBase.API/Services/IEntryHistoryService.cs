﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface IEntryHistoryService
    {
        IEnumerable<EntryHistoryResponse> GetListEntryHistory(int studentId);

        Boolean CreateEntry(int idstu, EntryHistoryRequest passEntry);

        Boolean DeleteEntrybyId(int entryid);

        Boolean UpdateEntrybyId(int id,EntryHistoryRequest model);

        EntryHistoryResponse GetEntrybyId(int entryid);
    }
}
