﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface IEduBackgroundService
    {
        IEnumerable<EduBackgroundRespone> GetListEduBackgroud(int studentId);

        Boolean CreateBackground(int stuid, EduBackgroundRequest model);

        Boolean DeleteBackgroundbyId(int eduid);

        Boolean UpdateBackgroundbyId(int id, EduBackgroundRequest model);

        EduBackgroundRespone GetBackgroundbyId(int eduid);
    }
}
