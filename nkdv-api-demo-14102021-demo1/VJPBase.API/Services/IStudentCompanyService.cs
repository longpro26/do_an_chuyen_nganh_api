﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface IStudentCompanyService
    {
        IEnumerable<StudentCompanyResponse> GetListStudentCompany(int studentId);

        Boolean CreateStudentCompany(int stuid, StudentCompanyRequest model);

        Boolean DeleteStudentCompanybyId(int comid);

        Boolean UpdateStudentCompanybyId(int id, StudentCompanyRequest model);

        StudentCompanyResponse GetStudentCompanybyId(int comid);
    }
}
