﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.API.Models.Response.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface ICostService
    {
        CostResponse GetCostbyId(int costId);
        Boolean InsertCost(CostRequest model);
        Boolean UpdateCost(int id, CostRequest model);
        bool CheckUpdate(int id);
        List<TypeCostRespon> GetTypeCost();
    }
}
