﻿using BCryptNet = BCrypt.Net.BCrypt;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using VJPBase.API.Models.Response;
using VJPBase.API.Models.Requests;
using NKVD.Entities.Models;

namespace VJPBase.API.Services
{
    public interface IUserService
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model, string ipAddress);
        AuthenticateResponse RefreshToken(string token, string ipAddress);
        void RevokeToken(string token, string ipAddress);
        IEnumerable<User1> GetAll();
        User1 GetById(int id);
        User1 GetCurrentUser();
    }
}
