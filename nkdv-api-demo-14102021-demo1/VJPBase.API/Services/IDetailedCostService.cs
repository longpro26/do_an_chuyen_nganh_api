﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface IDetailedCostService
    {
        IEnumerable <DetailedCostsResponse> GetAllDetailedCost(int studentId);
        DetailedCostsResponse GetDetailedCostbyId(int detailedcostdId);
        Boolean InsertDetailedCost(DetailedCostsRequest model);
        Boolean UpdateDetailedCost(int id, DetailedCostsRequest model);
        Boolean DeleteDetailedCost(int detailedcostdId);
    }
}
