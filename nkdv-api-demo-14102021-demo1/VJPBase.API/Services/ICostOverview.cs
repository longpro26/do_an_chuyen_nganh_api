﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using NKVD.API.Models.Response.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface ICostOverview
    {
        List<CostOverview2Response> GetAllCostovervieww();
        List<CostOverview2Response> SearchName(CostOverviewRequest mode);

        List<Grades> Grades();
        List<CostOverview2Response> FilterOverview(FilterOverviewRequest mode);
        bool RemoveCostOverview(RemoverCostOverview mode);
        List<CostOverview2Response> UpdateCostOverview(UpdateCostOverviewRequest mode);
    }
}
