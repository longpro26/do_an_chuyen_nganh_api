﻿using NKVD.API.Models.Requests;
using NKVD.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NKVD.API.Services
{
    public interface IForgotPassword
    {
        public string SendCode(ForgotPasswordEmailRequest request);
        public bool XacnhanUser(string email, string token);
        public bool ResetPassword(ChangePasswordRequest request);
    }
}
