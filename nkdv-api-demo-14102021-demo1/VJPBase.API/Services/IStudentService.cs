﻿using NKVD.API.Models.Requests;
using NKVD.API.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VJPBase.API.Models.Requests;
using VJPBase.API.Models.Response;

namespace NKVD.API.Services
{
    public interface IStudentService
    {
        StudentInfoDetailResponse GetbyID(int id);

        int CreateStudent(StudentRequest model);

        Boolean UpdateStudent(int stuid,StudentRequest model);
        List<StudentInfoResponse> GetTuitionOfStudent();
        List<StudentInfoResponse> GetTuitionOfStudent1();
        List<StudentInfoResponse> GetTuitionOfStudent2();

        List<StudentResponse> GetCostStudent();
        List<StudentResponse> GetTuitionDebtStudent();

        List<StudentInfoResponse> GetAllStudentTotal();
        public List<StudentInfoResponse> SearchName(FindStuOfNameRequest name);
        bool RemoveStudent(RemoveStudentRequest remove);
        public List<StudentInfoResponse> SearchNameByTuition2(FindStuOfNameRequest name);
        public List<StudentInfoResponse> SearchNameByTuition1(FindStuOfNameRequest name);
        public bool ChangePassStudent(ChangePassStudentRequest request);
        public int CountStudent();

    }
}
