﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NKVD.API.Migrations
{
    public partial class initdatabase3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Users1_TeacherID",
                table: "Users1",
                column: "TeacherID",
                unique: true,
                filter: "[TeacherID] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Users1_UserAdminID",
                table: "Users1",
                column: "UserAdminID",
                unique: true,
                filter: "[UserAdminID] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Users1_Teachers_TeacherID",
                table: "Users1",
                column: "TeacherID",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users1_UserAdmins_UserAdminID",
                table: "Users1",
                column: "UserAdminID",
                principalTable: "UserAdmins",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users1_Teachers_TeacherID",
                table: "Users1");

            migrationBuilder.DropForeignKey(
                name: "FK_Users1_UserAdmins_UserAdminID",
                table: "Users1");

            migrationBuilder.DropIndex(
                name: "IX_Users1_TeacherID",
                table: "Users1");

            migrationBuilder.DropIndex(
                name: "IX_Users1_UserAdminID",
                table: "Users1");
        }
    }
}
