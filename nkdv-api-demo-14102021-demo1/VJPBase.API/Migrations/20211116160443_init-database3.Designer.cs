﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using VJPBase.API.Helpers;

namespace NKVD.API.Migrations
{
    [DbContext(typeof(NKDVDbContext))]
    [Migration("20211116160443_init-database3")]
    partial class initdatabase3
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.9")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("NKVD.Entities.Models.Company", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("BusinessProfession")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Representative")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("TaxCode")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Companys");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Cost", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("ContractSigningDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Note")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("TotalCostOfStudentId")
                        .HasColumnType("int");

                    b.Property<double>("TotalPrice")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.HasIndex("TotalCostOfStudentId")
                        .IsUnique();

                    b.ToTable("Costs");
                });

            modelBuilder.Entity("NKVD.Entities.Models.DetailedCost", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Collector")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("CostOfStudentId")
                        .HasColumnType("int");

                    b.Property<DateTime>("DateOfLing")
                        .HasColumnType("datetime2");

                    b.Property<string>("Note")
                        .HasColumnType("nvarchar(max)");

                    b.Property<double>("Piece")
                        .HasColumnType("float");

                    b.Property<string>("Submitter")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Type")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("CostOfStudentId");

                    b.ToTable("DetailedCosts");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Document", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Link")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Note")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("ReceivedDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Receiver")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("StudentId")
                        .HasColumnType("int");

                    b.Property<string>("Submitter")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Type")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("StudentId");

                    b.ToTable("Documents");
                });

            modelBuilder.Entity("NKVD.Entities.Models.EduBackground", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Form")
                        .HasColumnType("datetime2");

                    b.Property<string>("SchoolName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("StudentId")
                        .HasColumnType("int");

                    b.Property<DateTime>("To")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("StudentId");

                    b.ToTable("EduBackgrounds");
                });

            modelBuilder.Entity("NKVD.Entities.Models.FamilyMember", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("BirthDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Job")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Relationship")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("StudentId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("StudentId");

                    b.ToTable("FamilyMembers");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Grade", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("OpenDate")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.ToTable("Grades");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Guardian", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("CompanyOfGuardianId")
                        .HasColumnType("int");

                    b.Property<int?>("GuardianOfStudentId")
                        .HasColumnType("int");

                    b.Property<string>("GuardianPosition")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("IdentificationCardOfGuardianId")
                        .HasColumnType("int");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RepresentativePosition")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("TaxCode")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("WorkPlace")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("WorkingTime")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("CompanyOfGuardianId")
                        .IsUnique()
                        .HasFilter("[CompanyOfGuardianId] IS NOT NULL");

                    b.HasIndex("GuardianOfStudentId")
                        .IsUnique()
                        .HasFilter("[GuardianOfStudentId] IS NOT NULL");

                    b.HasIndex("IdentificationCardOfGuardianId")
                        .IsUnique()
                        .HasFilter("[IdentificationCardOfGuardianId] IS NOT NULL");

                    b.ToTable("Guardians");
                });

            modelBuilder.Entity("NKVD.Entities.Models.IdentificationCard", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("BirthPlace")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Birthday")
                        .HasColumnType("datetime2");

                    b.Property<string>("CurrentAddress")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime2");

                    b.Property<string>("HometownAddress")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("IdIdentificationCard")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("IssuedBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PermanentAddress")
                        .HasColumnType("nvarchar(max)");

                    b.Property<byte>("Sex")
                        .HasColumnType("tinyint");

                    b.HasKey("Id");

                    b.ToTable("IdentificationCards");
                });

            modelBuilder.Entity("NKVD.Entities.Models.InfoInJapan", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("AddressType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("AreaCode")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("InfoInJapanOfStudentId")
                        .HasColumnType("int");

                    b.Property<string>("Majors")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("SchoolName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Specialized")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("InfoInJapanOfStudentId")
                        .IsUnique();

                    b.ToTable("InfoInJapans");
                });

            modelBuilder.Entity("NKVD.Entities.Models.PassEntryHistory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("EntryDate")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("OutputDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("PassportValidity")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Purpose")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("StudentId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("StudentId");

                    b.ToTable("PassEntryHistorys");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Review", b =>
                {
                    b.Property<int>("TeacherId")
                        .HasColumnType("int");

                    b.Property<int>("StudentId")
                        .HasColumnType("int");

                    b.Property<string>("AttendanceRate")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("HomeworkRate")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<string>("LearningAttitude")
                        .HasColumnType("nvarchar(max)");

                    b.Property<float>("Score")
                        .HasColumnType("real");

                    b.HasKey("TeacherId", "StudentId");

                    b.HasIndex("StudentId");

                    b.ToTable("Reviews");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Student", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("FacebookUrl")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Gmail")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("IdentificationCardOfStudentId")
                        .HasColumnType("int");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("IdentificationCardOfStudentId")
                        .IsUnique();

                    b.ToTable("Students");
                });

            modelBuilder.Entity("NKVD.Entities.Models.StudentCompany", b =>
                {
                    b.Property<int>("StudentId")
                        .HasColumnType("int");

                    b.Property<int>("CompanyId")
                        .HasColumnType("int");

                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.HasKey("StudentId", "CompanyId");

                    b.HasIndex("CompanyId");

                    b.ToTable("StudentCompanys");
                });

            modelBuilder.Entity("NKVD.Entities.Models.StudentGrade", b =>
                {
                    b.Property<int>("StudentId")
                        .HasColumnType("int");

                    b.Property<int>("GradeId")
                        .HasColumnType("int");

                    b.HasKey("StudentId", "GradeId");

                    b.HasIndex("GradeId");

                    b.ToTable("StudentGrades");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Teacher", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Teachers");
                });

            modelBuilder.Entity("NKVD.Entities.Models.TeacherGrade", b =>
                {
                    b.Property<int>("TeacherId")
                        .HasColumnType("int");

                    b.Property<int>("GradeId")
                        .HasColumnType("int");

                    b.HasKey("TeacherId", "GradeId");

                    b.HasIndex("GradeId");

                    b.ToTable("TeacherGrades");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Type", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Types");
                });

            modelBuilder.Entity("NKVD.Entities.Models.TypeCost", b =>
                {
                    b.Property<int>("DetailCostId")
                        .HasColumnType("int");

                    b.Property<int>("TypeId")
                        .HasColumnType("int");

                    b.Property<int?>("DetailedCostsId")
                        .HasColumnType("int");

                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.HasKey("DetailCostId", "TypeId");

                    b.HasIndex("DetailedCostsId");

                    b.HasIndex("TypeId");

                    b.ToTable("TypeCosts");
                });

            modelBuilder.Entity("NKVD.Entities.Models.User1", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Role")
                        .HasColumnType("int");

                    b.Property<int?>("StudentID")
                        .HasColumnType("int");

                    b.Property<int?>("TeacherID")
                        .HasColumnType("int");

                    b.Property<string>("Token")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("UserAdminID")
                        .HasColumnType("int");

                    b.Property<string>("UserName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("UserStaffID")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("StudentID");

                    b.HasIndex("TeacherID")
                        .IsUnique()
                        .HasFilter("[TeacherID] IS NOT NULL");

                    b.HasIndex("UserAdminID")
                        .IsUnique()
                        .HasFilter("[UserAdminID] IS NOT NULL");

                    b.HasIndex("UserStaffID")
                        .IsUnique()
                        .HasFilter("[UserStaffID] IS NOT NULL");

                    b.ToTable("Users1");
                });

            modelBuilder.Entity("NKVD.Entities.Models.UserAdmin", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Dateofbirth")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FullName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("UserAdmins");
                });

            modelBuilder.Entity("NKVD.Entities.Models.UserStaff", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Dateofbirth")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FullName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("UserStaffs");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Visa", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DateRange")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("ExpirationDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("IssuedByVisa")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Type")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("VisaOfStudentId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("VisaOfStudentId")
                        .IsUnique();

                    b.ToTable("Visas");
                });

            modelBuilder.Entity("NKVD.Entities.Models.WorkExperience", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Form")
                        .HasColumnType("datetime2");

                    b.Property<string>("Position")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("StudentId")
                        .HasColumnType("int");

                    b.Property<DateTime>("To")
                        .HasColumnType("datetime2");

                    b.Property<string>("WorkName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("StudentId");

                    b.ToTable("WorkExperiences");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Cost", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithOne("Cost")
                        .HasForeignKey("NKVD.Entities.Models.Cost", "TotalCostOfStudentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.DetailedCost", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithMany("DetailedCosts")
                        .HasForeignKey("CostOfStudentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Document", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithMany("Documents")
                        .HasForeignKey("StudentId");

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.EduBackground", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithMany("EduBackgrounds")
                        .HasForeignKey("StudentId");

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.FamilyMember", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithMany("FamilyMembers")
                        .HasForeignKey("StudentId");

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Guardian", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Company", "Company")
                        .WithOne("Guardian")
                        .HasForeignKey("NKVD.Entities.Models.Guardian", "CompanyOfGuardianId");

                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithOne("Guardian")
                        .HasForeignKey("NKVD.Entities.Models.Guardian", "GuardianOfStudentId");

                    b.HasOne("NKVD.Entities.Models.IdentificationCard", "IdentificationCard")
                        .WithOne("Guardian")
                        .HasForeignKey("NKVD.Entities.Models.Guardian", "IdentificationCardOfGuardianId");

                    b.Navigation("Company");

                    b.Navigation("IdentificationCard");

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.InfoInJapan", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithOne("InfoInJapan")
                        .HasForeignKey("NKVD.Entities.Models.InfoInJapan", "InfoInJapanOfStudentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.PassEntryHistory", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithMany("PassEntryHistorys")
                        .HasForeignKey("StudentId");

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Review", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithMany("Reviews")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("NKVD.Entities.Models.Teacher", "Teacher")
                        .WithMany("Reviews")
                        .HasForeignKey("TeacherId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Student");

                    b.Navigation("Teacher");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Student", b =>
                {
                    b.HasOne("NKVD.Entities.Models.IdentificationCard", "IdentificationCard")
                        .WithOne("Student")
                        .HasForeignKey("NKVD.Entities.Models.Student", "IdentificationCardOfStudentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("IdentificationCard");
                });

            modelBuilder.Entity("NKVD.Entities.Models.StudentCompany", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Company", "Company")
                        .WithMany("StudentCompanys")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithMany("StudentCompanys")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Company");

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.StudentGrade", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Grade", "Grade")
                        .WithMany("StudentGrades")
                        .HasForeignKey("GradeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithMany("StudentGrades")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Grade");

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.TeacherGrade", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Grade", "Grade")
                        .WithMany("TeacherGrades")
                        .HasForeignKey("GradeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("NKVD.Entities.Models.Teacher", "Teacher")
                        .WithMany("TeacherGrades")
                        .HasForeignKey("TeacherId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Grade");

                    b.Navigation("Teacher");
                });

            modelBuilder.Entity("NKVD.Entities.Models.TypeCost", b =>
                {
                    b.HasOne("NKVD.Entities.Models.DetailedCost", "DetailedCosts")
                        .WithMany("TypeCosts")
                        .HasForeignKey("DetailedCostsId");

                    b.HasOne("NKVD.Entities.Models.Type", "Types")
                        .WithMany("TypeCosts")
                        .HasForeignKey("TypeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("DetailedCosts");

                    b.Navigation("Types");
                });

            modelBuilder.Entity("NKVD.Entities.Models.User1", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithMany()
                        .HasForeignKey("StudentID");

                    b.HasOne("NKVD.Entities.Models.Teacher", "Teacher")
                        .WithOne("User1")
                        .HasForeignKey("NKVD.Entities.Models.User1", "TeacherID");

                    b.HasOne("NKVD.Entities.Models.UserAdmin", "UserAdmin")
                        .WithOne("User1")
                        .HasForeignKey("NKVD.Entities.Models.User1", "UserAdminID");

                    b.HasOne("NKVD.Entities.Models.UserStaff", "UserStaff")
                        .WithOne("User1")
                        .HasForeignKey("NKVD.Entities.Models.User1", "UserStaffID");

                    b.OwnsMany("NKVD.Entities.Models.RefreshToken1", "RefreshTokens1", b1 =>
                        {
                            b1.Property<int>("Id")
                                .ValueGeneratedOnAdd()
                                .HasColumnType("int")
                                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                            b1.Property<DateTime>("Created")
                                .HasColumnType("datetime2");

                            b1.Property<string>("CreatedByIp")
                                .HasColumnType("nvarchar(max)");

                            b1.Property<DateTime>("Expires")
                                .HasColumnType("datetime2");

                            b1.Property<string>("ReasonRevoked")
                                .HasColumnType("nvarchar(max)");

                            b1.Property<string>("ReplacedByToken")
                                .HasColumnType("nvarchar(max)");

                            b1.Property<DateTime?>("Revoked")
                                .HasColumnType("datetime2");

                            b1.Property<string>("RevokedByIp")
                                .HasColumnType("nvarchar(max)");

                            b1.Property<string>("Token")
                                .HasColumnType("nvarchar(max)");

                            b1.Property<int>("User1Id")
                                .HasColumnType("int");

                            b1.HasKey("Id");

                            b1.HasIndex("User1Id");

                            b1.ToTable("RefreshToken1");

                            b1.WithOwner()
                                .HasForeignKey("User1Id");
                        });

                    b.Navigation("RefreshTokens1");

                    b.Navigation("Student");

                    b.Navigation("Teacher");

                    b.Navigation("UserAdmin");

                    b.Navigation("UserStaff");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Visa", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithOne("Visa")
                        .HasForeignKey("NKVD.Entities.Models.Visa", "VisaOfStudentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.WorkExperience", b =>
                {
                    b.HasOne("NKVD.Entities.Models.Student", "Student")
                        .WithMany("WorkExperiences")
                        .HasForeignKey("StudentId");

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Company", b =>
                {
                    b.Navigation("Guardian");

                    b.Navigation("StudentCompanys");
                });

            modelBuilder.Entity("NKVD.Entities.Models.DetailedCost", b =>
                {
                    b.Navigation("TypeCosts");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Grade", b =>
                {
                    b.Navigation("StudentGrades");

                    b.Navigation("TeacherGrades");
                });

            modelBuilder.Entity("NKVD.Entities.Models.IdentificationCard", b =>
                {
                    b.Navigation("Guardian");

                    b.Navigation("Student");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Student", b =>
                {
                    b.Navigation("Cost");

                    b.Navigation("DetailedCosts");

                    b.Navigation("Documents");

                    b.Navigation("EduBackgrounds");

                    b.Navigation("FamilyMembers");

                    b.Navigation("Guardian");

                    b.Navigation("InfoInJapan");

                    b.Navigation("PassEntryHistorys");

                    b.Navigation("Reviews");

                    b.Navigation("StudentCompanys");

                    b.Navigation("StudentGrades");

                    b.Navigation("Visa");

                    b.Navigation("WorkExperiences");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Teacher", b =>
                {
                    b.Navigation("Reviews");

                    b.Navigation("TeacherGrades");

                    b.Navigation("User1");
                });

            modelBuilder.Entity("NKVD.Entities.Models.Type", b =>
                {
                    b.Navigation("TypeCosts");
                });

            modelBuilder.Entity("NKVD.Entities.Models.UserAdmin", b =>
                {
                    b.Navigation("User1");
                });

            modelBuilder.Entity("NKVD.Entities.Models.UserStaff", b =>
                {
                    b.Navigation("User1");
                });
#pragma warning restore 612, 618
        }
    }
}
